<?php

/**
 * @file
 * Contains uw_admission_requirements.module.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\user\Entity\Role;

/**
 * Implements hook_help().
 */
function uw_admission_requirements_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the uw_admission_requirements module.
    case 'help.page.uw_admission_requirements':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('MUR Admission Requirements module') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_theme().
 */
function uw_admission_requirements_theme() {
  $theme = [];
  $theme['requirement_entity'] = [
    'render element' => 'elements',
    'file' => 'requirement_entity.page.inc',
    'template' => 'requirement_entity',
  ];

  // Custom twig templates for requirements pages.
  $theme['admission_requirement'] = [
    'variables' => ['content' => NULL, 'attributes' => []],
  ];

  $theme['non_typical_requirement'] = [
    'variables' => ['content' => NULL, 'attributes' => []],
  ];
  return $theme;
}

/**
 * Implements hook_theme_suggestions_HOOK().
 */
function uw_admission_requirements_theme_suggestions_requirement_entity(array $variables) {
  $suggestions = [];
  $entity = $variables['elements']['#requirement_entity'];
  $sanitized_view_mode = strtr($variables['elements']['#view_mode'], '.', '_');

  $suggestions[] = 'requirement_entity__' . $sanitized_view_mode;
  $suggestions[] = 'requirement_entity__' . $entity->bundle();
  $suggestions[] = 'requirement_entity__' . $entity->bundle() . '__' . $sanitized_view_mode;
  $suggestions[] = 'requirement_entity__' . $entity->id();
  $suggestions[] = 'requirement_entity__' . $entity->id() . '__' . $sanitized_view_mode;
  return $suggestions;
}

/**
 * Implements hook_modules_installed().
 */
function uw_admission_requirements_modules_installed($modules, $is_syncing) {

}

/**
 * Implements hook_preprocess().
 *
 * This is used to add the clipboard icon to the menu item.
 */
function uw_admission_requirements_preprocess_page(&$variables) {
  // If we're displaying the admin toolbar then add our menu icon library.
  if ($variables['is_admin']) {
    $variables['#attached']['library'][] = 'uw_admission_requirements/menu';
  }

  // Remove breadcrumbs from admission pages.
  $route_name = \Drupal::routeMatch()->getRouteObject();
  if ($route_name->getPath() == '/requirements/{program_codename}/{system_codename}/{apib}') {
    $variables['page']['breadcrumbs'] = [];
  }
}

/**
 * All the required permission to UW roles.
 */
function uw_admission_requirements_assign_default_permissions() {
  $permissions = [
    'add requirement entity entities',
    'administer requirement entity entities',
    'delete requirement entity entities',
    'edit requirement entity entities',
    'view published requirement entity entities',
    'view unpublished requirement entity entities',
    'view all requirement entity revisions',
    'revert all requirement entity revisions',
    'delete all requirement entity revisions',
    'add program entity entities',
    'administer program entity entities',
    'delete program entity entities',
    'edit program entity entities',
    'view published program entity entities',
    'view unpublished program entity entities',
    'view all program entity revisions',
    'revert all program entity revisions',
    'delete all program entity revisions',
    'add system entity entities',
    'administer system entity entities',
    'delete system entity entities',
    'edit system entity entities',
    'view published system entity entities',
    'view unpublished system entity entities',
    'view all system entity revisions',
    'revert all system entity revisions',
    'delete all system entity revisions',
    'administer requirement entity type',
  ];

  // Allow site managers to administer.
  $role_object = Role::load('uw_role_site_manager');
  foreach ($permissions as $permission) {
    $role_object->grantPermission($permission);
  }
  $role_object->save();

  // Allow site owners to administer.
  $role_object = Role::load('uw_role_site_owner');
  foreach ($permissions as $permission) {
    $role_object->grantPermission($permission);
  }
  $role_object->save();
}

/**
 * Removes un-needed configuration data.
 */
function uw_admission_requirements_remove_settings() {
  \Drupal::configFactory()->getEditable('layout_builder_browser.layout_builder_browser_block.uw_admission_requirements')->delete();
}
