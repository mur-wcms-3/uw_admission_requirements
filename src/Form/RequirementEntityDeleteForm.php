<?php

namespace Drupal\uw_admission_requirements\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Requirement entity entities.
 *
 * @ingroup uw_admission_requirements
 */
class RequirementEntityDeleteForm extends ContentEntityDeleteForm {


}
