<?php

namespace Drupal\uw_admission_requirements\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class Requirement Entity Type Form.
 */
class RequirementEntityTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $requirement_entity_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $requirement_entity_type->label(),
      '#description' => $this->t("Label for the Requirement entity type."),
      '#required' => TRUE,
    ];

    $form['page_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Page Label'),
      '#maxlength' => 255,
      '#default_value' => $requirement_entity_type->page_label(),
      '#description' => $this->t("Page label for the Requirement entity type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $requirement_entity_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\uw_admission_requirements\Entity\RequirementEntityType::load',
      ],
      '#disabled' => !$requirement_entity_type->isNew(),
    ];

    $form['weight'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Weight'),
      '#maxlength' => 3,
      '#default_value' => $requirement_entity_type->weight(),
      '#description' => $this->t("Order weight for the Requirement Type."),
      '#required' => TRUE,
    ];

    $form['is_special'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Is Special'),
      '#default_value' => $requirement_entity_type->is_special(),
      '#description' => $this->t("Special requirements are explicitly templated and have special rules for when they show up on a page."),
      '#required' => FALSE,
    ];
    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $requirement_entity_type = $this->entity;
    $status = $requirement_entity_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Requirement entity type.', [
          '%label' => $requirement_entity_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Requirement entity type.', [
          '%label' => $requirement_entity_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($requirement_entity_type->toUrl('collection'));
  }

}
