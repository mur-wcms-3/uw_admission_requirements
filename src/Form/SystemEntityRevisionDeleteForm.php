<?php

namespace Drupal\uw_admission_requirements\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for deleting a System entity revision.
 *
 * @ingroup uw_admission_requirements
 */
class SystemEntityRevisionDeleteForm extends ConfirmFormBase {

  /**
   * The System entity revision.
   *
   * @var \Drupal\uw_admission_requirements\Entity\SystemEntityInterface
   */
  protected $revision;

  /**
   * The System entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $systemEntityStorage;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->systemEntityStorage = $container->get('entity_type.manager')->getStorage('system_entity');
    $instance->connection = $container->get('database');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'system_entity_revision_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the revision from %revision-date?', [
      '%revision-date' => format_date($this->revision->getRevisionCreationTime()),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.system_entity.version_history', ['system_entity' => $this->revision->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $system_entity_revision = NULL) {
    $this->revision = $this->systemEntityStorage->loadRevision($system_entity_revision);
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->systemEntityStorage->deleteRevision($this->revision->getRevisionId());

    $this->logger('content')->notice('System entity: deleted %title revision %revision.',
      [
        '%title' => $this->revision->label(),
        '%revision' => $this->revision->getRevisionId(),
      ]
    );
    $this->messenger()->addMessage($this->t('Revision from %revision-date of System entity %title has been deleted.',
      [
        '%revision-date' => format_date($this->revision->getRevisionCreationTime()),
        '%title' => $this->revision->label(),
      ]
    ));
    $form_state->setRedirect(
      'entity.system_entity.canonical',
       ['system_entity' => $this->revision->id()]
    );
    if ($this->connection->query('SELECT COUNT(DISTINCT vid) FROM {system_entity_field_revision} WHERE id = :id', [':id' => $this->revision->id()])->fetchField() > 1) {
      $form_state->setRedirect(
        'entity.system_entity.version_history',
         ['system_entity' => $this->revision->id()]
      );
    }
  }

}
