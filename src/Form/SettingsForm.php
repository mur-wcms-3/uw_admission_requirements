<?php

namespace Drupal\uw_admission_requirements\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\uw_admission_requirements\FixtureService;
use Drupal\Core\State\State;

/**
 * Class Settings Form.
 */
class SettingsForm extends FormBase {

  /**
   * Configuration service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $config;

  /**
   * State.
   *
   * @var Drupal\Core\State\State
   */
  protected $state;

  /**
   * Fixture service.
   *
   * @var Drupal\uw_admission_requirements\FixtureService
   */
  protected $fixture;

  /**
   * Messenger service.
   *
   * @var Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Entity type manager.
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('messenger'),
      $container->get('uw_admission_requirements.fixtures'),
      $container->get('state'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    MessengerInterface $messenger,
    FixtureService $fixture,
    State $state,
    EntityTypeManagerInterface $entityTypeManager
  ) {
    $this->config = $configFactory->getEditable('uw_admission_requirements.settings');
    $this->messenger = $messenger;
    $this->fixture = $fixture;
    $this->state = $state;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'uw_admission_requirements_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config->get('admin.config');

    $form['header'] = [
      '#markup' => '<h2>Non-typical Student Type Settings</h2>',
    ];

    // Vertical tabs.
    $form['non_typical'] = [
      '#type' => 'vertical_tabs',
      '#default_tab' => 'edit-ged',
    ];

    // GED Content.
    $form['ged'] = [
      '#type' => 'details',
      '#title' => $this->t('GED'),
      '#group' => 'non_typical',
    ];

    $form['ged']['ged_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Form Label'),
      '#default_value' => $config['non-typical-student-type']['ged'],
    ];

    $ged_page_title = $this->state->get('uw_admission_requirements.ged_page_title');
    $form['ged']['ged_page_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Page Title'),
      '#default_value' => $ged_page_title,
    ];

    $ged_page_content = $this->state->get('uw_admission_requirements.ged_page_content');
    $form['ged']['ged_page_content'] = [
      '#type' => 'text_format',
      '#format' => 'uw_tf_standard',
      '#title' => $this->t('Page Content'),
      '#default_value' => $ged_page_content ?: '',
    ];

    $ged_use_redirect = $this->state->get('uw_admission_requirements.ged_use_redirect');
    $form['ged']['ged_use_redirect'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Override content and redirect to an existing web page.'),
      '#default_value' => $ged_use_redirect,
    ];

    $ged_node_id = $this->state->get('uw_admission_requirements.ged_redirect', NULL);
    if ($ged_node_id) {
      $ged_node = $this->entityTypeManager->getStorage('node')->load($ged_node_id);
    }
    else {
      $ged_node = NULL;
    }
    $form['ged']['ged_redirect'] = [
      '#type' => 'entity_autocomplete',
      '#title' => 'Redirect Page',
      '#default_value' => $ged_node ?: '',
      '#target_type' => 'node',
      '#selection_settings' => [
        'target_bundles' => ['page', 'uw_ct_web_page'],
      ],
      '#states' => [
        'disabled' => [
          ':input[name="ged_use_redirect"]' => ['checked' => FALSE],
        ],
        'required' => [
          ':input[name="mature_use_redirect"]' => ['checked' => TRUE],
        ],
      ],
    ];

    // Home Schooled.
    $form['home_schooled'] = [
      '#type' => 'details',
      '#title' => $this->t('Home Schooled Students'),
      '#group' => 'non_typical',
    ];

    $form['home_schooled']['home_schooled_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Form Label'),
      '#default_value' => $config['non-typical-student-type']['home-schooled-students'],
    ];

    $home_schooled_page_title = $this->state->get('uw_admission_requirements.home_schooled_page_title');
    $form['home_schooled']['home_schooled_page_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Page Title'),
      '#default_value' => $home_schooled_page_title,
    ];

    $home_schooled_page_content = $this->state->get('uw_admission_requirements.home_schooled_page_content');
    $form['home_schooled']['home_schooled_page_content'] = [
      '#type' => 'text_format',
      '#format' => 'uw_tf_standard',
      '#title' => $this->t('Page Content'),
      '#default_value' => $home_schooled_page_content,
    ];

    $home_schooled_use_redirect = $this->state->get('uw_admission_requirements.home_schooled_use_redirect');
    $form['home_schooled']['home_schooled_use_redirect'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Override content and redirect to an existing web page.'),
      '#default_value' => $home_schooled_use_redirect,
    ];

    $home_schooled_node_id = $this->state->get('uw_admission_requirements.home_schooled_redirect', NULL);
    if ($home_schooled_node_id) {
      $home_schooled_node = $this->entityTypeManager->getStorage('node')->load($home_schooled_node_id);
    }
    else {
      $home_schooled_node = NULL;
    }
    $form['home_schooled']['home_schooled_redirect'] = [
      '#type' => 'entity_autocomplete',
      '#title' => 'Redirect Page',
      '#default_value' => $home_schooled_node ?: '',
      '#target_type' => 'node',
      '#selection_settings' => [
        'target_bundles' => ['page', 'uw_ct_web_page'],
      ],
      '#states' => [
        'disabled' => [
          ':input[name="home_schooled_use_redirect"]' => ['checked' => FALSE],
        ],
        'required' => [
          ':input[name="mature_use_redirect"]' => ['checked' => TRUE],
        ],
      ],
    ];

    // Mature student content.
    $form['mature'] = [
      '#type' => 'details',
      '#title' => $this->t('Mature Student'),
      '#group' => 'non_typical',
    ];

    $form['mature']['mature_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Form Label'),
      '#default_value' => $config['non-typical-student-type']['mature-student'],
    ];

    $mature_page_title = $this->state->get('uw_admission_requirements.mature_page_title');
    $form['mature']['mature_page_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Page Title'),
      '#default_value' => $mature_page_title,
    ];

    $mature_page_content = $this->state->get('uw_admission_requirements.mature_page_content');
    $form['mature']['mature_page_content'] = [
      '#type' => 'text_format',
      '#format' => 'uw_tf_standard',
      '#title' => $this->t('Page Content'),
      '#default_value' => $mature_page_content,
    ];

    $mature_use_redirect = $this->state->get('uw_admission_requirements.mature_use_redirect');
    $form['mature']['mature_use_redirect'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Override content and redirect to an existing web page.'),
      '#default_value' => $mature_use_redirect,
    ];

    $mature_node_id = $this->state->get('uw_admission_requirements.mature_redirect', NULL);
    if ($mature_node_id) {
      $mature_node = $this->entityTypeManager
        ->getStorage('node')
        ->load($mature_node_id);
    }
    else {
      $mature_node = NULL;
    }
    $form['mature']['mature_redirect'] = [
      '#type' => 'entity_autocomplete',
      '#title' => 'Redirect Page',
      '#default_value' => $mature_node ?: '',
      '#target_type' => 'node',
      '#selection_settings' => [
        'target_bundles' => ['page', 'uw_ct_web_page'],
      ],
      '#states' => [
        'disabled' => [
          ':input[name="mature_use_redirect"]' => ['checked' => FALSE],
        ],
        'required' => [
          ':input[name="mature_use_redirect"]' => ['checked' => TRUE],
        ],
      ],
    ];

    // Submit Action.
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save Changes'),
      '#suffix' => '<br><br>',
    ];

    $form['header2'] = [
      '#markup' => '<hr><br><h2>Initialization Settings</h2>',
    ];

    // Initialization tabs.
    $form['other'] = [
      '#type' => 'vertical_tabs',
      '#default_tab' => 'system',
    ];

    // Systems.
    $form['systems'] = [
      '#type' => 'details',
      '#title' => $this->t('Systems'),
      '#group' => 'other',
    ];

    $form['systems']['sub-header'] = [
      '#markup' => '<h3>Initialize Systems</h3><span>Initializes default systems in a single operation.</span>',
    ];

    $form['systems']['create_system_defaults'] = [
      '#type' => 'submit',
      '#value' => $this->t('Create Default Systems'),
      '#submit' => ['::createSystemDefaults'],
      '#prefix' => "<br><br>",
      '#suffix' => "<br><br>",
    ];

    $form['programs'] = [
      '#type' => 'details',
      '#title' => $this->t('Programs'),
      '#group' => 'other',
    ];

    $form['programs']['sub-header'] = [
      '#markup' => '<h3>Initialize Programs</h3><span>Initializes default programs in a single operation.</span>',
    ];

    $form['programs']['create_program_defaults'] = [
      '#type' => 'submit',
      '#value' => $this->t('Create Default Programs'),
      '#submit' => ['::createProgramDefaults'],
      '#prefix' => "<br><br>",
      '#suffix' => "<br><br>",
    ];

    $form['requirements'] = [
      '#type' => 'details',
      '#title' => $this->t('Requirements'),
      '#group' => 'other',
    ];

    $form['requirements']['sub-header'] = [
      '#markup' => '<h3>Initialize Requirements</h3><span>Initialize requirement types individually.</span>',
    ];

    $form['requirements']['create_general_requirements'] = [
      '#type' => 'submit',
      '#value' => $this->t('Create General Requirements'),
      '#submit' => ['::createGeneralRequirements'],
      '#prefix' => "<br><br>",
      '#suffix' => "<br><br>",
    ];

    $form['requirements']['create_country_requirements'] = [
      '#type' => 'submit',
      '#value' => $this->t('Create Country Requirements'),
      '#submit' => ['::createCountryRequirements'],
      '#suffix' => "<br><br>",
    ];

    $form['requirements']['create_program_average_requirements'] = [
      '#type' => 'submit',
      '#value' => $this->t('Create Program Average Requirements'),
      '#submit' => ['::createProgramAverageRequirements'],
      '#suffix' => "<br><br>",
    ];

    $form['requirements']['create_recommended_courses'] = [
      '#type' => 'submit',
      '#value' => $this->t('Create Recommended Courses'),
      '#submit' => ['::createRecommendedCourses'],
      '#suffix' => "<br><br>",
    ];

    $form['requirements']['create_national_required_courses'] = [
      '#type' => 'submit',
      '#value' => $this->t('Create Required Courses (National)'),
      '#submit' => ['::createNationalRequiredCourses'],
      '#suffix' => "<br><br>",
    ];

    $form['requirements']['create_intl_required_courses'] = [
      '#type' => 'submit',
      '#value' => $this->t('Create Required Courses (International)'),
      '#submit' => ['::createIntlRequiredCourses'],
      '#suffix' => "<br><br>",
    ];

    $form['requirements']['create_program_transfer_requirements'] = [
      '#type' => 'submit',
      '#value' => $this->t('Create Transfer Requirements'),
      '#submit' => ['::createProgramTransferRequirements'],
      '#suffix' => "<br><br>",
    ];

    $form['requirements']['create_ap_requirements'] = [
      '#type' => 'submit',
      '#value' => $this->t('Create AP Requirements'),
      '#submit' => ['::createApRequirements'],
      '#suffix' => "<br><br>",
    ];

    $form['requirements']['sub-header-2'] = [
      '#markup' => '<h3>Initialize All Requirements</h3><span>Initializes everything above in one single operation.</span>',
    ];

    $form['requirements']['create_all_requirements'] = [
      '#type' => 'submit',
      '#value' => $this->t('Create All Requirement Defaults'),
      '#submit' => ['::createAllRequirements'],
      '#suffix' => "<br><br>",
      '#prefix' => "<br><br>",
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save GED Settings.
    $ged_label = $form_state->getValue('ged_label');
    $this->config->set('admin.config.non-typical-student-type.ged', $ged_label);

    $ged_page_title = $form_state->getValue('ged_page_title');
    $this->state->set('uw_admission_requirements.ged_page_title', $ged_page_title);

    $ged_page_content = $form_state->getValue('ged_page_content');
    $this->state->set('uw_admission_requirements.ged_page_content', $ged_page_content['value']);

    $ged_use_redirect = $form_state->getValue('ged_use_redirect');
    $this->state->set('uw_admission_requirements.ged_use_redirect', $ged_use_redirect);

    $ged_redirect = $form_state->getValue('ged_redirect');
    $this->state->set('uw_admission_requirements.ged_redirect', $ged_redirect);

    // Save Home Schooled Settings.
    $home_schooled_label = $form_state->getValue('home_schooled_label');
    $this->config->set('admin.config.non-typical-student-type.home-schooled-students', $home_schooled_label);

    $home_schooled_page_title = $form_state->getValue('home_schooled_page_title');
    $this->state->set('uw_admission_requirements.home_schooled_page_title', $home_schooled_page_title);

    $home_schooled_page_content = $form_state->getValue('home_schooled_page_content');
    $this->state->set('uw_admission_requirements.home_schooled_page_content', $home_schooled_page_content['value']);

    $home_schooled_use_redirect = $form_state->getValue('home_schooled_use_redirect');
    $this->state->set('uw_admission_requirements.home_schooled_use_redirect', $home_schooled_use_redirect);

    $home_schooled_redirect = $form_state->getValue('home_schooled_redirect');
    $this->state->set('uw_admission_requirements.home_schooled_redirect', $home_schooled_redirect);

    // Save Mature Student Settings.
    $mature_label = $form_state->getValue('mature_label');
    $this->config->set('admin.config.non-typical-student-type.mature-student', $mature_label);

    $mature_page_title = $form_state->getValue('mature_page_title');
    $this->state->set('uw_admission_requirements.mature_page_title', $mature_page_title);

    $mature_page_content = $form_state->getValue('mature_page_content');
    $this->state->set('uw_admission_requirements.mature_page_content', $mature_page_content['value']);

    $mature_use_redirect = $form_state->getValue('mature_use_redirect');
    $this->state->set('uw_admission_requirements.mature_use_redirect', $mature_use_redirect);

    $mature_redirect = $form_state->getValue('mature_redirect');
    $this->state->set('uw_admission_requirements.mature_redirect', $mature_redirect);

    // Save our setting changes.
    $this->config->save();
  }

  /**
   * Populates default systems.
   */
  public function createSystemDefaults(array &$form, FormStateInterface $form_state) {
    // Get the custom fixture service.
    $fixture_service = $this->fixture;
    // Call method to create the systems.
    $fixture_service->createDefaultSystems();
    // Display a message on completion.
    $this->messenger->addStatus($this->t('Default system values have been populated.'));
  }

  /**
   * Populates default general reqs.
   */
  public function createGeneralRequirements(array &$form, FormStateInterface $form_state) {
    // Get the custom fixture service.
    $fixture_service = $this->fixture;
    // Call method to create the systems.
    $fixture_service->createGeneralRequirements();
    // Display a message on completion.
    $this->messenger->addStatus($this->t('General requirements for systems have been populated.'));
  }

  /**
   * Populates default programs.
   */
  public function createProgramDefaults(array &$form, FormStateInterface $form_state) {
    // Get the custom fixture service.
    $fixture_service = $this->fixture;
    // Call method to create the systems.
    $fixture_service->createDefaultPrograms();
    // Display a message on completion.
    $this->messenger->addStatus($this->t('Default program values have been populated.'));
  }

  /**
   * Populates recommended courses.
   */
  public function createRecommendedCourses(array &$form, FormStateInterface $form_state) {
    // Get the custom fixture service.
    $fixture_service = $this->fixture;
    // Call method to create the systems.
    $fixture_service->createRecommendedCourses();
    // Display a message on completion.
    $this->messenger->addStatus($this->t('Default recommended courses have been populated.'));
  }

  /**
   * Populates default country reqs.
   */
  public function createCountryRequirements(array &$form, FormStateInterface $form_state) {
    // Get the custom fixture service.
    $fixture_service = $this->fixture;
    // Call method to create the systems.
    $fixture_service->createCountryRequirements();
    // Display a message on completion.
    $this->messenger->addStatus($this->t('Country requirements have been populated.'));
  }

  /**
   * Populates default program averages.
   */
  public function createProgramAverageRequirements(array &$form, FormStateInterface $form_state) {
    // Get the custom fixture service.
    $fixture_service = $this->fixture;
    // Call method to create the systems.
    $fixture_service->createProgramAverageRequirements();
    // Display a message on completion.
    $this->messenger->addStatus($this->t('Program Average requirements have been populated.'));
  }

  /**
   * Populates default international required courses.
   */
  public function createIntlRequiredCourses(array &$form, FormStateInterface $form_state) {
    // Get the custom fixture service.
    $fixture_service = $this->fixture;
    // Call method to create the systems.
    $fixture_service->createInternationalRequiredCourses();
    // Display a message on completion.
    $this->messenger->addStatus($this->t('International Required Courses have been populated.'));
  }

  /**
   * Populates default national required courses.
   */
  public function createNationalRequiredCourses(array &$form, FormStateInterface $form_state) {
    // Get the custom fixture service.
    $fixture_service = $this->fixture;
    // Call method to create the systems.
    $fixture_service->createNationalRequiredCourses();
    // Display a message on completion.
    $this->messenger->addStatus($this->t('National Required Courses have been populated.'));
  }

  /**
   * Populates default program transfer requirements.
   */
  public function createProgramTransferRequirements(array &$form, FormStateInterface $form_state) {
    // Get the custom fixture service.
    $fixture_service = $this->fixture;
    // Call method to create the systems.
    $fixture_service->createProgramTransferRequirements();
    // Display a message on completion.
    $this->messenger->addStatus($this->t('Program Requirements have been populated.'));
  }

  /**
   * Populates default AP requirements.
   */
  public function createApRequirements(array &$form, FormStateInterface $form_state) {
    // Get the custom fixture service.
    $fixture_service = $this->fixture;
    // Call method to create the systems.
    $fixture_service->createApRequirements();
    // Display a message on completion.
    $this->messenger->addStatus($this->t('AP Requirements have been populated.'));
  }

  /**
   * Populates all default requirements.
   */
  public function createAllRequirements(array &$form, FormStateInterface $form_state) {
    // Get the custom fixture service.
    $fixture_service = $this->fixture;
    // Call method to create the systems.
    $fixture_service->createDefaultSystems();
    $fixture_service->createGeneralRequirements();
    $fixture_service->createDefaultPrograms();
    $fixture_service->createRecommendedCourses();
    $fixture_service->createCountryRequirements();
    $fixture_service->createProgramAverageRequirements();
    $fixture_service->createInternationalRequiredCourses();
    $fixture_service->createNationalRequiredCourses();
    $fixture_service->createProgramTransferRequirements();
    $fixture_service->createApRequirements();
    // Display a message on completion.
    $this->messenger->addStatus($this->t('All requirements initialization completed.'));
  }

}
