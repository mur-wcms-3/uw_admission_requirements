<?php

namespace Drupal\uw_admission_requirements\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting System entity entities.
 *
 * @ingroup uw_admission_requirements
 */
class SystemEntityDeleteForm extends ContentEntityDeleteForm {


}
