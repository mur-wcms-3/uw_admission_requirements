<?php

namespace Drupal\uw_admission_requirements\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Admission Requirements Form Class.
 */
class AdmissionRequirementsForm extends FormBase {

  /**
   * Configuration service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $config;

  /**
   * Entity type manager.
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $configFactory, EntityTypeManagerInterface $entityTypeManager) {
    $this->config = $configFactory->get('uw_admission_requirements.settings');
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'uw_admission_requirements_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config->get('admin.config');

    // Student Type.
    $form['student_type'] = [
      '#type' => 'select',
      '#title' => $config['student-type-label'] ?: "I am",
      '#title_display' => 'before',
      '#options' => [
        'high-school-student' => $config['student-type']['high-school-student'] ?: "A high school student / graduate",
        'college-transfer' => $config['student-type']['college-transfer'] ?: "A college student / graduate",
        'university-transfer' => $config['student-type']['university-transfer'] ?: "A university student / graduate",
        'other-student' => $config['student-type']['other-student'] ?: "Other",
      ],
      '#default_value' => 'high-school-student',
      '#required' => TRUE,
      '#field_prefix' => '<br>',
    ];

    // Non-typical student type.
    $form['non_typical_student_type'] = [
      '#type' => 'select',
      '#options' => [
        'ged' => $config['non-typical-student-type']['ged'] ?: "General Educational Development (GED) student",
        'home-schooled-students' => $config['non-typical-student-type']['home-schooled-students'] ?: "Home-schooled student",
        'mature-student' => $config['non-typical-student-type']['mature-student'] ?: "Mature student",
      ],
      '#default_value' => 'ged',
      '#required' => FALSE,
      '#field_prefix' => '<br>',
      '#states' => [
        'visible' => [
          ':input[name="student_type"]' => ['value' => 'other-student'],
        ],
        'required' => [
          ':input[name="student_type"]' => ['value' => 'other-student'],
        ],
      ],
    ];

    // Domestic Systems (Provinces).
    $form['system_domestic'] = [
      '#type' => 'select',
      '#options' => static::getSystems(),
      '#default_value' => 'ontario',
      '#required' => FALSE,
      '#title' => $config['province-label'] ?: "I attend/attended high school in",
      '#title_display' => 'before',
      '#field_prefix' => '<br>',
      '#states' => [
        'invisible' => [
          ':input[name="student_type"]' => ['value' => 'other-student'],
        ],
        'required' => [
          ':input[name="student_type"]' => ['!value' => 'other-student'],
        ],
      ],
    ];

    // International Systems (Country/System of Study)
    $form['system_international'] = [
      '#type' => 'select',
      '#options' => static::getSystems(TRUE),
      '#required' => FALSE,
      '#title' => $config['system-label'] ?: "Country/System of study:",
      '#title_display' => 'before',
      '#field_prefix' => '<br>',
      '#states' => [
        'visible' => [
          ':input[name="system_domestic"]' => ['value' => 'other'],
          ':input[name="student_type"]' => ['!value' => 'other-student'],
        ],
        'required' => [
          ':input[name="system_domestic"]' => ['value' => 'other'],
          ':input[name="student_type"]' => ['!value' => 'other-student'],
        ],

      ],
    ];

    // Countries.
    $form['country'] = [
      '#type' => 'select',
      '#options' => static::getCountries(),
      '#required' => FALSE,
      '#title' => $config['country-label'] ?: "Country:",
      '#title_display' => 'before',
      '#field_prefix' => '<br>',
      '#states' => [
        'visible' => [
          ':input[name="system_domestic"]' => ['value' => 'other'],
          ':input[name="system_international"]' => ['value' => 'international'],
          ':input[name="student_type"]' => ['!value' => 'other-student'],
        ],
        'required' => [
          ':input[name="system_domestic"]' => ['value' => 'other'],
          ':input[name="system_international"]' => ['value' => 'international'],
          ':input[name="student_type"]' => ['!value' => 'other-student'],
        ],

      ],
    ];

    // AP IB select.
    $form['apib'] = [
      '#type' => 'checkboxes',
      '#options' => [
        'ap' => $config['apib-type']['ap'] ?: "Advanced Placement (AP) courses",
        'ib' => $config['apib-type']['ib'] ?: "International Baccalaureate (IB) courses or diploma",
      ],
      '#required' => FALSE,
      '#attributes' => ['class' => ['apib']],
      '#title' => $config['apib-label'] ?: "I am taking",
      '#title_display' => 'before',
      '#states' => [
        'visible' => [
          ':input[name="student_type"]' => ['value' => 'high-school-student'],
          ':input[name="system_domestic"]' => ['!value' => 'other'],
        ],
        'optional' => [
          ':input[name="student_type"]' => ['value' => 'other-student'],
          ':input[name="system_domestic"]' => ['value' => 'other'],
        ],
      ],
    ];

    $form['program'] = [
      '#type' => 'select',
      '#options' => static::getPrograms(),
      '#required' => FALSE,
      '#title' => $config['program-label'] ?: "I'm interested in",
      '#title_display' => 'before',
      '#field_prefix' => '<br>',
      '#states' => [
        'visible' => [
          ':input[name="student_type"]' => ['!value' => 'other-student'],
        ],
        'required' => [
          ':input[name="student_type"]' => ['!value' => 'other-student'],
        ],
      ],
    ];

    $form['#attached']['library'] = [
      'uw_admission_requirements/checkbox-radio-behaviour',
    ];

    // Don't see your program listed? link.
    $form['program_list_link'] = Link::fromTextAndUrl($config['program-list-label'] ?: "Don't see your program listed?",
      Url::fromUri($config['program-list-url']))->toRenderable();

    $form['spacer'] = [
      '#markup' => '<br><br>',
    ];

    // Submit button.
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('View Requirements'),
    ];

    $form['spacer2'] = [
      '#markup' => '<br><br>',
    ];
    return $form;
  }

  /**
   * Implements form validation.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Temporarily store all form errors.
    $form_errors = $form_state->getErrors();
    // Clear the form errors.
    $form_state->clearErrors();

    // Get values to check.
    $student_type = $form_state->getValue('student_type');
    $system_domestic = $form_state->getValue('system_domestic');
    $system_international = $form_state->getValue('system_international');

    // If other-student type, we don't set any other values.
    if ($student_type == 'other-student') {
      $form_state->unsetValue('apib');
      $form_state->unsetValue('system_domestic');
      $form_state->unsetValue('system_international');
      $form_state->unsetValue('program');
      $form_state->unsetValue('country');

      $form_errors = [];
    }

    // If not an 'other-student' type, do not require non typical student type.
    if ($student_type != 'other-student') {
      $form_state->unsetValue('non_typical_student_type');
      unset($form_errors['non_typical_student_type']);
    }

    // Remove apib for non domestic highschool student types.
    if ($student_type != 'high-school-student' || $system_domestic == 'other') {
      $form_state->unsetValue('apib');
      unset($form_errors['apib']);
    }

    // Remove requirements for country if a domestic system is selected.
    if ($system_domestic != 'other') {
      $form_state->unsetValue('system_international');
      unset($form_errors['system_international']);
      $form_state->unsetValue('country');
      unset($form_errors['country']);
    }

    // Remove country and apib if a recognized international system is selected.
    if (!$form_state->isValueEmpty('system_international') && $system_international != 'international') {
      $form_state->unsetValue('country');
      unset($form_errors['country']);
      $form_state->unsetValue('apib');
      unset($form_errors['apib']);
    }

    // Re-apply the remaining form error messages.
    foreach ($form_errors as $name => $error_message) {
      $form_state->setErrorByName($name, $error_message);
    }
  }

  /**
   * Implements a form submit handler.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Uncomment this to debug easier.
    // $form_state->disableRedirect(TRUE);
    // Get values to check.
    $student_type = $form_state->getValue('student_type');
    $non_typical_student_type = $form_state->getValue('non_typical_student_type');
    $system_domestic = $form_state->getValue('system_domestic');
    $system_international = $form_state->getValue('system_international');
    $program = $form_state->getValue('program');
    $country = $form_state->getValue('country');
    // Reduce the apib array to just the set value.
    $apib = $form_state->getValue('apib');
    if (!empty($apib)) {
      $apib = array_reduce($apib, function ($carry, $item) {
        return $carry = ($item == "0" ? $carry : $item);
      });
    }

    // Check for 'other' student types, redirect to those pages if set.
    switch ($student_type) {

      case 'other-student':
        $route_name = 'uw_admission_requirements.admissions_requirements_non_typical_requirements';
        $route_params = ['student_type' => $non_typical_student_type];
        $form_state->setRedirect($route_name, $route_params);
        break;

      case 'high-school-student':
        // National Highschool Students.
        if ($system_domestic != 'other') {
          $route_name = 'uw_admission_requirements.admissions_requirements_system_requirements';
          // Redirect to IB system if 'ib' checkbox was selected.
          // Ontario exception in place.
          if ($apib == 'ib' && $system_domestic != 'ontario') {
            $system_domestic = 'international-system';
          }
          elseif ($apib == 'ib' && $system_domestic == 'ontario') {
            $apib = NULL;
          }
          $route_params = [
            'program_codename' => $program,
            'system_codename' => $system_domestic,
            'apib' => $apib,
          ];
          $form_state->setRedirect($route_name, $route_params);
          break;
        }

        // Non National Systems - highschool students.
        if ($system_domestic == 'other' && $system_international != 'international') {
          $route_name = 'uw_admission_requirements.admissions_requirements_system_requirements';
          $route_params = [
            'program_codename' => $program,
            'system_codename' => $system_international,
            'apib' => $apib,
          ];
          $form_state->setRedirect($route_name, $route_params);
          break;
        }

        // Other international highschool students.
        if ($system_international == 'international') {
          $route_name = 'uw_admission_requirements.admissions_requirements_international_requirements';
          $route_params = [
            'program_codename' => $program,
            'country' => $country,
          ];
          $form_state->setRedirect($route_name, $route_params);
          break;
        }
        break;

      case 'college-transfer':
        // National college transfer.
        if ($system_domestic != 'other') {
          $route_name = 'uw_admission_requirements.admissions_requirements_system_college_transfer_requirements';
          $route_params = [
            'program_codename' => $program,
            'system_codename' => $system_domestic,
            'country' => NULL,
          ];
          $form_state->setRedirect($route_name, $route_params);
          break;
        }
        // Systems college transfer.
        if ($system_domestic == 'other' && $system_international != 'international') {
          $route_name = 'uw_admission_requirements.admissions_requirements_system_college_transfer_requirements';
          $route_params = [
            'program_codename' => $program,
            'system_codename' => $system_international,
            'country' => NULL,
          ];
          $form_state->setRedirect($route_name, $route_params);
          break;
        }
        // Other international college transfer.
        if ($system_international == 'international') {
          $route_name = 'uw_admission_requirements.admissions_requirements_international_college_transfer_requirements';
          $route_params = [
            'program_codename' => $program,
            'system_codename' => 'international',
            'country' => $country,
          ];
          $form_state->setRedirect($route_name, $route_params);
          break;
        }
        break;

      case 'university-transfer':
        // National university transfer.
        if ($system_domestic != 'other') {
          $route_name = 'uw_admission_requirements.admissions_requirements_system_university_transfer_requirements';
          $route_params = [
            'program_codename' => $program,
            'system_codename' => $system_domestic,
            'country' => NULL,
          ];
          $form_state->setRedirect($route_name, $route_params);
          break;
        }
        // Systems university transfer.
        if ($system_domestic == 'other' && $system_international != 'international') {
          $route_name = 'uw_admission_requirements.admissions_requirements_system_university_transfer_requirements';
          $route_params = [
            'program_codename' => $program,
            'system_codename' => $system_international,
            'country' => NULL,
          ];
          $form_state->setRedirect($route_name, $route_params);
          break;
        }
        // Other international university transfer.
        if ($system_international == 'international') {
          $route_name = 'uw_admission_requirements.admissions_requirements_international_university_transfer_requirements';
          $route_params = [
            'program_codename' => $program,
            'system_codename' => NULL,
            'country' => $country,
          ];
          $form_state->setRedirect($route_name, $route_params);
          break;
        }
        break;
    }
  }

  /**
   * Returns a list of active systems.
   *
   * @param bool $is_international
   *   Set true for retrieving international systems.
   *
   * @return array
   *   Returns a list of systems
   */
  public function getSystems($is_international = FALSE) {
    // Get all published systems without is_international checked.
    $system_ids = $this->entityTypeManager->getStorage('system_entity')->getQuery()
      ->condition('status', NodeInterface::PUBLISHED)
      ->condition('is_international', $is_international)
      ->sort('name', 'ASC')
      ->execute();

    $systems = $this->entityTypeManager
      ->getStorage('system_entity')
      ->loadMultiple($system_ids);

    $options = [];
    foreach ($systems as $system) {
      $options[$system->codename->value] = $system->name->value;
    }

    if ($is_international) {
      unset($options['international']);
      $options['international'] = 'Other International';
    }
    else {
      $options['other'] = 'Outside of Canada';
    }

    return $options;
  }

  /**
   * Retrieves a list of active programs for use in the form dropdown.
   *
   * @return array
   *   Returns programs
   */
  public function getPrograms() {
    // Get all published systems without is_international checked.
    $program_ids = $this->entityTypeManager->getStorage('program_entity')->getQuery()
      ->condition('status', NodeInterface::PUBLISHED)
      ->sort('name', 'ASC')
      ->execute();

    $programs = $this->entityTypeManager
      ->getStorage('program_entity')
      ->loadMultiple($program_ids);

    $options = [];
    foreach ($programs as $program) {
      $options[$program->codename->value] = $program->name->value;
    }

    return $options;
  }

  /**
   * Retrieves a list of countries from the country bundle.
   *
   * @return array
   *   Countries.
   */
  public function getCountries() {
    // Get all published systems without is_international checked.
    $country_ids = $this->entityTypeManager->getStorage('requirement_entity')->getQuery()
      ->condition('status', NodeInterface::PUBLISHED)
      ->condition('type', 'country')
      ->sort('name', 'ASC')
      ->execute();

    $countries = $this->entityTypeManager
      ->getStorage('requirement_entity')
      ->loadMultiple($country_ids);

    $options = [];
    foreach ($countries as $country) {
      $options[$country->codename->value] = $country->name->value;
    }

    return $options;
  }

}
