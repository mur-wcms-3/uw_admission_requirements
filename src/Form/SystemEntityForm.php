<?php

namespace Drupal\uw_admission_requirements\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for System entity edit forms.
 *
 * @ingroup uw_admission_requirements
 */
class SystemEntityForm extends ContentEntityForm {

  /**
   * The current user account.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    $instance = parent::create($container);
    $instance->account = $container->get('current_user');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    // Remove revision fields for now.
    unset($form['user_id']);
    unset($form['new_revision']);
    unset($form['revision_log_message']);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Run parent validation first, else the entity not validated.
    parent::validateForm($form, $form_state);

    // Validate Alias value is unique and contains no spaces.
    $codename = $form_state->getValue(['codename'])[0]['value'];
    if (preg_match("/[^a-z0-9_-]+/", $codename)) {
      $form_state->setErrorByName('codename', $this->t('The codename must contain only lowercase letters, numbers, dashes, and underscores.'));
    }

    // If system is new, or system has a changed value, check for duplicates.
    if ((!$this->entity->isNew() && $codename != $this->entity->codename->value) || $this->entity->isNew()) {
      $result = $this->entityTypeManager->getStorage('system_entity')->getQuery()
        ->condition('codename', $codename)
        ->execute();

      if (!empty($result)) {
        $form_state->setErrorByName('codename', $this->t('Codename value must be unique.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    // Save as a new revision if requested to do so.
    if (!$form_state->isValueEmpty('new_revision') && $form_state->getValue('new_revision') != FALSE) {
      $entity->setNewRevision();

      // If a new revision is created, save the current user as revision author.
      $entity->setRevisionCreationTime($this->time->getRequestTime());
      $entity->setRevisionUserId($this->account->id());
    }
    else {
      $entity->setNewRevision(FALSE);
    }

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label System entity.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label System entity.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.system_entity.collection');
  }

}
