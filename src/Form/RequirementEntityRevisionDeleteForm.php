<?php

namespace Drupal\uw_admission_requirements\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for deleting a Requirement entity revision.
 *
 * @ingroup uw_admission_requirements
 */
class RequirementEntityRevisionDeleteForm extends ConfirmFormBase {

  /**
   * The Requirement entity revision.
   *
   * @var \Drupal\uw_admission_requirements\Entity\RequirementEntityInterface
   */
  protected $revision;

  /**
   * The Requirement entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $requirementEntityStorage;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->requirementEntityStorage = $container->get('entity_type.manager')->getStorage('requirement_entity');
    $instance->connection = $container->get('database');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'requirement_entity_revision_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the revision from %revision-date?', [
      '%revision-date' => format_date($this->revision->getRevisionCreationTime()),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.requirement_entity.version_history', ['requirement_entity' => $this->revision->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $requirement_entity_revision = NULL) {
    $this->revision = $this->requirementEntityStorage->loadRevision($requirement_entity_revision);
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->requirementEntityStorage->deleteRevision($this->revision->getRevisionId());

    $this->logger('content')->notice('Requirement entity: deleted %title revision %revision.',
      [
        '%title' => $this->revision->label(),
        '%revision' => $this->revision->getRevisionId(),
      ]);
    $this->messenger()->addMessage($this->t('Revision from %revision-date of Requirement entity %title has been deleted.',
      [
        '%revision-date' => format_date($this->revision->getRevisionCreationTime()),
        '%title' => $this->revision->label(),
      ]));
    $form_state->setRedirect(
      'entity.requirement_entity.canonical',
       ['requirement_entity' => $this->revision->id()]
    );
  }

}
