<?php

namespace Drupal\uw_admission_requirements\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Program entity entities.
 *
 * @ingroup uw_admission_requirements
 */
class ProgramEntityDeleteForm extends ContentEntityDeleteForm {


}
