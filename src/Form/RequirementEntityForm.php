<?php

namespace Drupal\uw_admission_requirements\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for Requirement entity edit forms.
 *
 * @ingroup uw_admission_requirements
 */
class RequirementEntityForm extends ContentEntityForm {

  /**
   * The current user account.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * Entity type manager.
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Route match.
   *
   * @var Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $routeMatch;

  /**
   * Current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    $instance = parent::create($container);
    $instance->account = $container->get('current_user');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->routeMatch = $container->get('current_route_match');
    $instance->request = $container->get('request_stack')->getCurrentRequest();
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    // If program id is passed in the query string, pre-load the program into
    // the program id field of the form.
    $params = $this->request->query->all();
    if (array_key_exists('program_id', $params)) {
      $pid = $params['program_id'];
      $program_entity = $this->entityTypeManager->getStorage('program_entity')->load($pid);
      $form['program_id']['widget'][0]['target_id']['#default_value'] = $program_entity;
    }

    // For new country requirements, default system to international.
    if ($this->entity->isNew()) {
      $bundle = $this->routeMatch
        ->getParameters()
        ->get('requirement_entity_type')
        ->id();
      // Make sure this is for the country type.
      if ($bundle == 'country') {
        // Change the description title to General Requirements for clarity.
        $form['description']['widget'][0]['#title'] = 'General Requirements';

        // Get the id for international.
        $sid = $this->entityTypeManager->getStorage('system_entity')->getQuery()
          ->condition('codename', 'international')
          ->execute();
        // Load the entity and set it as default.
        $system_entity = $this->entityTypeManager
          ->getStorage('system_entity')
          ->load(key($sid));
        // Set the default value.
        $form['system_id']['widget'][0]['target_id']['#default_value'] = $system_entity;
      }
    }

    // Remove revision fields for now.
    unset($form['user_id']);
    unset($form['new_revision']);
    unset($form['revision_log_message']);

    return $form;
  }

  /**
   * Validates form.
   *
   * @param array $form
   *   Form.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Run parent validation first, else the entity not validated.
    parent::validateForm($form, $form_state);

    // Validate codename value is unique and contains no spaces.
    if ($this->entity->bundle() == 'country') {
      $codename = $form_state->getValue(['codename'])[0]['value'];
      if (empty($codename)) {
        $form_state->setErrorByName(
          'codename',
          $this->t('Codename is a required field for country requirement type.')
        );
      }

      if (preg_match("/[^a-z0-9_-]+/", $codename)) {
        $form_state->setErrorByName(
          'codename',
          $this->t('The codename must contain only lowercase letters, numbers, dashes, and underscores.')
        );
      }

      if ((!$this->entity->isNew() && $codename != $this->entity->codename->value) || $this->entity->isNew()) {
        // Check for duplicate codename names.
        $result = $this->entityTypeManager->getStorage('requirement_entity')->getQuery()
          ->condition('codename', $codename)
          ->condition('type', $this->entity->bundle())
          ->execute();

        if (!empty($result)) {
          $form_state->setErrorByName('codename', $this->t('codename value must be unique.'));
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    // Save as a new revision if requested to do so.
    if (!$form_state->isValueEmpty('new_revision') && $form_state->getValue('new_revision') != FALSE) {
      $entity->setNewRevision();

      // If a new revision is created, save the current user as revision author.
      $entity->setRevisionCreationTime($this->time->getRequestTime());
      $entity->setRevisionUserId($this->account->id());
    }
    else {
      $entity->setNewRevision(FALSE);
    }

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Requirement entity.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Requirement entity.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('view.requirement_entities.overview');
  }

}
