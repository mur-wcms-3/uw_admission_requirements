<?php

namespace Drupal\uw_admission_requirements;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of System entity entities.
 *
 * @ingroup uw_admission_requirements
 */
class SystemEntityListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [
      'name' => [
        'data' => $this->t('Program'),
        'field' => 'name',
        'specifier' => 'name',
        'class' => [RESPONSIVE_PRIORITY_LOW],
      ],
      'codename' => [
        'data' => $this->t('URL Codename'),
        'field' => 'codename',
        'specifier' => 'codename',
        'class' => [RESPONSIVE_PRIORITY_LOW],
      ],
      'is_international' => [
        'data' => $this->t('Is International'),
        'field' => 'is_international',
        'specifier' => 'is_international',
        'class' => [RESPONSIVE_PRIORITY_LOW],
      ],
    ];

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.system_entity.edit_form',
      ['system_entity' => $entity->id()]
    );
    $row['codename'] = $entity->codename->value;
    $row['is_international'] = $entity->is_international->value ? 'Yes' : 'No';
    return $row + parent::buildRow($entity);
  }

}
