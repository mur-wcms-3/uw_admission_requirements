<?php

namespace Drupal\uw_admission_requirements;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Provides a listing of Requirement entity type entities.
 */
class RequirementEntityTypeListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Requirement Type');
    $header['page_label'] = $this->t('Page Label');
    $header['id'] = $this->t('Machine name');
    $header['weight'] = $this->t('Order weight');
    $header['is_special'] = $this->t('Special');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    $row['page_label'] = $entity->page_label();
    $row['id'] = $entity->id();
    $row['weight'] = $entity->weight();
    $row['is_special'] = $entity->is_special() ? 'Yes' : 'No';

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);

    if (isset($operations['edit'])) {
      $operations['edit']['title'] = $this->t('Edit Requirement Type');
    }

    if ($entity->access('administer requirement entity entities')) {
      $operations['list'] = [
        'title' => $this->t('List Requirements'),
        'weight' => 0,
        'url' => Url::fromRoute('view.requirement_entities.overview', [], ['query' => ['type' => $entity->id()]]),
      ];
    }

    $operations['add'] = [
      'title' => $this->t('Add Requirement'),
      'weight' => 10,
      'url' => Url::fromRoute('entity.requirement_entity.add_form', ['requirement_entity_type' => $entity->id()]),
    ];

    return $operations;
  }

}
