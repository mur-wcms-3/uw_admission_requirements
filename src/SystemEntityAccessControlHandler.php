<?php

namespace Drupal\uw_admission_requirements;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the System entity entity.
 *
 * @see \Drupal\uw_admission_requirements\Entity\SystemEntity.
 */
class SystemEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\uw_admission_requirements\Entity\SystemEntityInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished system entity entities');
        }

        return AccessResult::allowedIfHasPermission($account, 'view published system entity entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit system entity entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete system entity entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add system entity entities');
  }

}
