<?php

namespace Drupal\uw_admission_requirements;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\uw_admission_requirements\Entity\RequirementEntityInterface;

/**
 * Defines the storage handler class for Requirement entity entities.
 *
 * This extends the base storage class, adding required special handling for
 * Requirement entity entities.
 *
 * @ingroup uw_admission_requirements
 */
interface RequirementEntityStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Requirement entity revision IDs.
   *
   * @param \Drupal\uw_admission_requirements\Entity\RequirementEntityInterface $entity
   *   The Requirement entity entity.
   *
   * @return int[]
   *   Requirement entity revision IDs (in ascending order).
   */
  public function revisionIds(RequirementEntityInterface $entity);

  /**
   * Gets a list of revision IDs having a given user.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Requirement entity revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

}
