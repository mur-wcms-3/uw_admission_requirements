<?php

namespace Drupal\uw_admission_requirements;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\uw_admission_requirements\Entity\SystemEntityInterface;

/**
 * Defines the storage handler class for System entity entities.
 *
 * This extends the base storage class, adding required special handling for
 * System entity entities.
 *
 * @ingroup uw_admission_requirements
 */
class SystemEntityStorage extends SqlContentEntityStorage implements SystemEntityStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(SystemEntityInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {system_entity_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {system_entity_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

}
