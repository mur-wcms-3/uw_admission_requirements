<?php

namespace Drupal\uw_admission_requirements;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\uw_admission_requirements\Entity\ProgramEntityInterface;

/**
 * Defines the storage handler class for Program entity entities.
 *
 * This extends the base storage class, adding required special handling for
 * Program entity entities.
 *
 * @ingroup uw_admission_requirements
 */
class ProgramEntityStorage extends SqlContentEntityStorage implements ProgramEntityStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(ProgramEntityInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {program_entity_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {program_entity_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

}
