<?php

namespace Drupal\uw_admission_requirements;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\uw_admission_requirements\Entity\ProgramEntityInterface;

/**
 * Defines the storage handler class for Program entity entities.
 *
 * This extends the base storage class, adding required special handling for
 * Program entity entities.
 *
 * @ingroup uw_admission_requirements
 */
interface ProgramEntityStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Program entity revision IDs for a specific Program entity.
   *
   * @param \Drupal\uw_admission_requirements\Entity\ProgramEntityInterface $entity
   *   The Program entity entity.
   *
   * @return int[]
   *   Program entity revision IDs (in ascending order).
   */
  public function revisionIds(ProgramEntityInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Program entity author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Program entity revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

}
