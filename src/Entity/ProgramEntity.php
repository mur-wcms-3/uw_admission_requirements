<?php

namespace Drupal\uw_admission_requirements\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EditorialContentEntityBase;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Program entity entity.
 *
 * @ingroup uw_admission_requirements
 *
 * @ContentEntityType(
 *   id = "program_entity",
 *   label = @Translation("Program"),
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log_message",
 *   },
 *   handlers = {
 *     "storage" = "Drupal\uw_admission_requirements\ProgramEntityStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\uw_admission_requirements\ProgramEntityListBuilder",
 *     "views_data" = "Drupal\uw_admission_requirements\Entity\ProgramEntityViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\uw_admission_requirements\Form\ProgramEntityForm",
 *       "add" = "Drupal\uw_admission_requirements\Form\ProgramEntityForm",
 *       "edit" = "Drupal\uw_admission_requirements\Form\ProgramEntityForm",
 *       "delete" = "Drupal\uw_admission_requirements\Form\ProgramEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\uw_admission_requirements\ProgramEntityHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\uw_admission_requirements\ProgramEntityAccessControlHandler",
 *   },
 *   base_table = "program_entity",
 *   revision_table = "program_entity_revision",
 *   revision_data_table = "program_entity_field_revision",
 *   translatable = FALSE,
 *   admin_permission = "administer program entity entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/admission_requirements/program_entity/{program_entity}",
 *     "add-form" = "/admin/admission_requirements/program_entity/add",
 *     "edit-form" = "/admin/admission_requirements/program_entity/{program_entity}/edit",
 *     "delete-form" = "/admin/admission_requirements/program_entity/{program_entity}/delete",
 *     "version-history" = "/admin/admission_requirements/program_entity/{program_entity}/revisions",
 *     "revision" = "/admin/admission_requirements/program_entity/{program_entity}/revisions/{program_entity_revision}/view",
 *     "revision_revert" = "/admin/admission_requirements/program_entity/{program_entity}/revisions/{program_entity_revision}/revert",
 *     "revision_delete" = "/admin/admission_requirements/program_entity/{program_entity}/revisions/{program_entity_revision}/delete",
 *     "collection" = "/admin/admission_requirements/program_entity",
 *     "requirements" = "/admin/admission_requirements/program_entity/{program_entity}/requirements",
 *
 *   },
 *   field_ui_base_route = "program_entity.settings"
 * )
 */
class ProgramEntity extends EditorialContentEntityBase implements ProgramEntityInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    // If no revision author has been set explicitly,
    // make the program_entity owner the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCodename() {
    return $this->get('codename')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCodename($codename) {
    $this->set('codename', $codename);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Program entity entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 90,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 90,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Program entity entity.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -10,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->addConstraint('UniqueField');

    $fields['codename'] = BaseFieldDefinition::create('string')
      ->setLabel(t('URL Codename'))
      ->setDescription(t('A unique machine name for this item to be used in the requirement URL path. It must only contain lowercase letters, numbers, dashes and underscores.'))
      ->setRevisionable(FALSE)
      ->setSettings([
        'max_length' => 64,
      ])
      ->addConstraint('UniqueField')
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['description'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Description'))
      ->setDescription(t('Description of the program.'))
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => 10,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'text_default',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRevisionable(TRUE)
      ->setDefaultValue('')
      ->setRequired(FALSE);

    $fields['status']->setDescription(t('A boolean indicating whether the Program entity is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 99,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
