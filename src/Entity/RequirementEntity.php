<?php

namespace Drupal\uw_admission_requirements\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EditorialContentEntityBase;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Requirement entity entity.
 *
 * @ingroup uw_admission_requirements
 *
 * @ContentEntityType(
 *   id = "requirement_entity",
 *   label = @Translation("Requirement"),
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log_message",
 *   },
 *   bundle_label = @Translation("Requirement Type"),
 *   handlers = {
 *     "storage" = "Drupal\uw_admission_requirements\RequirementEntityStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\uw_admission_requirements\RequirementEntityListBuilder",
 *     "views_data" = "Drupal\uw_admission_requirements\Entity\RequirementEntityViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\uw_admission_requirements\Form\RequirementEntityForm",
 *       "add" = "Drupal\uw_admission_requirements\Form\RequirementEntityForm",
 *       "edit" = "Drupal\uw_admission_requirements\Form\RequirementEntityForm",
 *       "delete" = "Drupal\uw_admission_requirements\Form\RequirementEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\uw_admission_requirements\RequirementEntityHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\uw_admission_requirements\RequirementEntityAccessControlHandler",
 *   },
 *   base_table = "requirement_entity",
 *   revision_table = "requirement_entity_revision",
 *   revision_data_table = "requirement_entity_field_revision",
 *   translatable = FALSE,
 *   permission_granularity = "entity_type",
 *   admin_permission = "administer requirement entity entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/admission_requirements/requirement_entity/{requirement_entity}",
 *     "add-page" = "/admin/admission_requirements/requirement_entity/add",
 *     "add-form" = "/admin/admission_requirements/requirement_entity/add/{requirement_entity_type}",
 *     "edit-form" = "/admin/admission_requirements/requirement_entity/{requirement_entity}/edit",
 *     "delete-form" = "/admin/admission_requirements/requirement_entity/{requirement_entity}/delete",
 *     "version-history" = "/admin/admission_requirements/requirement_entity/{requirement_entity}/revisions",
 *     "revision" = "/admin/admission_requirements/requirement_entity/{requirement_entity}/revisions/{requirement_entity_revision}/view",
 *     "revision_revert" = "/admin/admission_requirements/requirement_entity/{requirement_entity}/revisions/{requirement_entity_revision}/revert",
 *     "revision_delete" = "/admin/admission_requirements/requirement_entity/{requirement_entity}/revisions/{requirement_entity_revision}/delete",
 *     "collection" = "/admin/admission_requirements/requirement_entity/overview",
 *   },
 *   bundle_entity_type = "requirement_entity_type",
 *   field_ui_base_route = "entity.requirement_entity_type.edit_form"
 * )
 */
class RequirementEntity extends EditorialContentEntityBase implements RequirementEntityInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    // If no revision author has been set explicitly,
    // make the requirement_entity owner the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getProgram() {
    return $this->get('program_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getProgramId() {
    return $this->get('program_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setProgramId($uid) {
    $this->set('program_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setProgram(ProgramEntityInterface $program) {
    $this->set('program_id', $program->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSystem() {
    return $this->get('system_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getSystemId() {
    return $this->get('system_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setSystemId($uid) {
    $this->set('system_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setSystem(SystemEntityInterface $system) {
    $this->set('system_id', $system->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Requirement entity entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 90,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 90,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Requirement entity entity.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -10,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['codename'] = BaseFieldDefinition::create('string')
      ->setLabel(t('URL Codename'))
      ->setDescription(t('A unique machine name for this item to be used in the requirement URL path. It must only contain lowercase letters, numbers, dashes and underscores.'))
      ->setRevisionable(FALSE)
      ->setSettings([
        'max_length' => 64,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['description'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Description'))
      ->setDescription(t('Description of the program.'))
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => 5,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'text_default',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRevisionable(TRUE)
      ->setDefaultValue('')
      ->setRequired(FALSE);

    $fields['program_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Program'))
      ->setDescription(t('The ID of program for the Requirement entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'program_entity')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'program_entity',
        'weight' => 10,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 10,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setRequired(FALSE)
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['system_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('System of Study'))
      ->setDescription(t('The ID of system for the Requirement entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'system_entity')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'system_entity',
        'weight' => 15,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 15,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setRequired(FALSE)
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status']->setDescription(t('A boolean indicating whether the Requirement entity is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 20,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
