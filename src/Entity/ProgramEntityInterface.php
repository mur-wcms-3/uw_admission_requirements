<?php

namespace Drupal\uw_admission_requirements\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Program entity entities.
 *
 * @ingroup uw_admission_requirements
 */
interface ProgramEntityInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Program entity name.
   *
   * @return string
   *   Name of the Program entity.
   */
  public function getName();

  /**
   * Sets the Program entity name.
   *
   * @param string $name
   *   The Program entity name.
   *
   * @return \Drupal\uw_admission_requirements\Entity\ProgramEntityInterface
   *   The called Program entity entity.
   */
  public function setName($name);

  /**
   * Gets the Program entity codename.
   *
   * @return string
   *   Codename of the Program entity.
   */
  public function getCodename();

  /**
   * Sets the Program entity codename.
   *
   * @param string $codename
   *   The Program entity codename.
   *
   * @return \Drupal\uw_admission_requirements\Entity\ProgramEntityInterface
   *   The called Program entity entity.
   */
  public function setCodename($codename);

  /**
   * Gets the Program entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Program entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Program entity creation timestamp.
   *
   * @param int $timestamp
   *   The Program entity creation timestamp.
   *
   * @return \Drupal\uw_admission_requirements\Entity\ProgramEntityInterface
   *   The called Program entity entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Program entity revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Program entity revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\uw_admission_requirements\Entity\ProgramEntityInterface
   *   The called Program entity entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Program entity revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Program entity revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\uw_admission_requirements\Entity\ProgramEntityInterface
   *   The called Program entity entity.
   */
  public function setRevisionUserId($uid);

}
