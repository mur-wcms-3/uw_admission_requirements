<?php

namespace Drupal\uw_admission_requirements\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining System entity entities.
 *
 * @ingroup uw_admission_requirements
 */
interface SystemEntityInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the System entity name.
   *
   * @return string
   *   Name of the System entity.
   */
  public function getName();

  /**
   * Sets the System entity name.
   *
   * @param string $name
   *   The System entity name.
   *
   * @return \Drupal\uw_admission_requirements\Entity\SystemEntityInterface
   *   The called System entity entity.
   */
  public function setName($name);

  /**
   * Gets the System entity codename.
   *
   * @return string
   *   Codename of the System entity.
   */
  public function getCodename();

  /**
   * Sets the System entity codename.
   *
   * @param string $codename
   *   The System entity codename.
   *
   * @return \Drupal\uw_admission_requirements\Entity\SystemEntityInterface
   *   The called System entity entity.
   */
  public function setCodename($codename);

  /**
   * Gets the System entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the System entity.
   */
  public function getCreatedTime();

  /**
   * Sets the System entity creation timestamp.
   *
   * @param int $timestamp
   *   The System entity creation timestamp.
   *
   * @return \Drupal\uw_admission_requirements\Entity\SystemEntityInterface
   *   The called System entity entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the System entity revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the System entity revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\uw_admission_requirements\Entity\SystemEntityInterface
   *   The called System entity entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the System entity revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the System entity revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\uw_admission_requirements\Entity\SystemEntityInterface
   *   The called System entity entity.
   */
  public function setRevisionUserId($uid);

}
