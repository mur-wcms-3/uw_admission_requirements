<?php

namespace Drupal\uw_admission_requirements\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Requirement entity type entities.
 */
interface RequirementEntityTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
