<?php

namespace Drupal\uw_admission_requirements\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Requirement entity entities.
 *
 * @ingroup uw_admission_requirements
 */
interface RequirementEntityInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Requirement entity name.
   *
   * @return string
   *   Name of the Requirement entity.
   */
  public function getName();

  /**
   * Sets the Requirement entity name.
   *
   * @param string $name
   *   The Requirement entity name.
   *
   * @return \Drupal\uw_admission_requirements\Entity\RequirementEntityInterface
   *   The called Requirement entity entity.
   */
  public function setName($name);

  /**
   * Gets the Requirement entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Requirement entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Requirement entity creation timestamp.
   *
   * @param int $timestamp
   *   The Requirement entity creation timestamp.
   *
   * @return \Drupal\uw_admission_requirements\Entity\RequirementEntityInterface
   *   The called Requirement entity entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Requirement entity revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Requirement entity revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\uw_admission_requirements\Entity\RequirementEntityInterface
   *   The called Requirement entity entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Requirement entity revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Requirement entity revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\uw_admission_requirements\Entity\RequirementEntityInterface
   *   The called Requirement entity entity.
   */
  public function setRevisionUserId($uid);

  /**
   * Returns the entity program entity.
   *
   * @return \Drupal\uw_admission_requirements\Entity\ProgramEntity
   *   The program entity.
   */
  public function getProgram();

  /**
   * Sets the entity program entity.
   *
   * @param \Drupal\uw_admission_requirements\Entity\ProgramEntityInterface $program
   *   The program entity.
   *
   * @return $this
   */
  public function setProgram(ProgramEntityInterface $program);

  /**
   * Returns the entity program ID.
   *
   * @return int|null
   *   The program ID, or NULL in case the program ID field has not been set on
   *   the entity.
   */
  public function getProgramId();

  /**
   * Sets the entity program ID.
   *
   * @param int $uid
   *   The program id.
   *
   * @return $this
   */
  public function setProgramId($uid);

  /**
   * Returns the entity system entity.
   *
   * @return \Drupal\uw_admission_requirements\Entity\SystemEntity
   *   The system entity.
   */
  public function getSystem();

  /**
   * Sets the entity system entity.
   *
   * @param \Drupal\uw_admission_requirements\Entity\SystemEntityInterface $system
   *   The system entity.
   *
   * @return $this
   */
  public function setSystem(SystemEntityInterface $system);

  /**
   * Returns the entity system ID.
   *
   * @return int|null
   *   The system ID, or NULL in case the system ID field has not been set on
   *   the entity.
   */
  public function getSystemId();

  /**
   * Sets the entity system ID.
   *
   * @param int $uid
   *   The system id.
   *
   * @return $this
   */
  public function setSystemId($uid);

}
