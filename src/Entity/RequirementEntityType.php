<?php
// phpcs:ignoreFile

namespace Drupal\uw_admission_requirements\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Requirement entity type entity.
 *
 * @ConfigEntityType(
 *   id = "requirement_entity_type",
 *   label = @Translation("Requirement Type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\uw_admission_requirements\RequirementEntityTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\uw_admission_requirements\Form\RequirementEntityTypeForm",
 *       "edit" = "Drupal\uw_admission_requirements\Form\RequirementEntityTypeForm",
 *       "delete" = "Drupal\uw_admission_requirements\Form\RequirementEntityTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\uw_admission_requirements\RequirementEntityTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "requirement_entity_type",
 *   admin_permission = "administer requirement entity type",
 *   bundle_of = "requirement_entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "weight" = "weight",
 *     "is_special" = "is_special"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *     "weight",
 *     "is_special"
 *   },
 *   links = {
 *     "canonical" = "/admin/admission_requirements/requirement_entity_type/{requirement_entity_type}",
 *     "add-form" = "/admin/admission_requirements/requirement_entity_type/add",
 *     "edit-form" = "/admin/admission_requirements/requirement_entity_type/{requirement_entity_type}/edit",
 *     "delete-form" = "/admin/admission_requirements/requirement_entity_type/{requirement_entity_type}/delete",
 *     "collection" = "/admin/admission_requirements/requirement_entity_type"
 *   }
 * )
 */
class RequirementEntityType extends ConfigEntityBundleBase implements RequirementEntityTypeInterface {

  /**
   * The Requirement entity type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Requirement entity type name.
   *
   * @var string
   */
  protected $page_label;

  /**
   * The Requirement entity type label.
   *
   * @var string
   */
  protected $label;

  /**
   * The Requirement entity type default weight.
   *
   * @var inte
   */
  protected $weight;

  /**
   * Marks the type as special and not a general requirement type.
   *
   * @var bool
   */
  protected $is_special;

  /**
   * Weight getter.
   *
   * @return int|null
   *   Weight.
   */
  public function weight() {
    return isset($this->weight) ? $this->weight : NULL;
  }

  /**
   * Name getter.
   *
   * @return string|null
   *   Page label.
   */
  public function page_label() {
    return isset($this->page_label) ? $this->page_label : NULL;
  }

  /**
   * Is_special getter.
   *
   * @return bool|null
   *   Is special.
   */
  public function is_special() {
    return isset($this->is_special) ? $this->is_special : NULL;
  }

}
