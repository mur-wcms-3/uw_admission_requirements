<?php

namespace Drupal\uw_admission_requirements\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Program entity entities.
 */
class ProgramEntityViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
