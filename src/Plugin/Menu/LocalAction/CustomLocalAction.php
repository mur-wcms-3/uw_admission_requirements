<?php

namespace Drupal\uw_admission_requirements\Plugin\Menu\LocalAction;

use Drupal\Core\Menu\LocalActionDefault;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Defines a local action plugin with route parameters.
 */
class CustomLocalAction extends LocalActionDefault {

  /**
   * Gets route params.
   *
   * @param Drupal\Core\Routing\RouteMatchInterface $route_match
   *   Route.
   *
   * @return array
   *   Array.
   */
  public function getRouteParameters(RouteMatchInterface $route_match) {
    if ($route_match->getRouteName() == 'entity.program_entity.edit_form') {
      $entity = $route_match->getParameter('program_entity');
      return [
        'program_id' => $entity->id(),
      ];
    }
    return [];
  }

}
