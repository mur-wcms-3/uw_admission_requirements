<?php

namespace Drupal\uw_admission_requirements;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\uw_admission_requirements\Entity\SystemEntityInterface;

/**
 * Defines the storage handler class for System entity entities.
 *
 * This extends the base storage class, adding required special handling for
 * System entity entities.
 *
 * @ingroup uw_admission_requirements
 */
interface SystemEntityStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of System entity revision IDs for a specific System entity.
   *
   * @param \Drupal\uw_admission_requirements\Entity\SystemEntityInterface $entity
   *   The System entity entity.
   *
   * @return int[]
   *   System entity revision IDs (in ascending order).
   */
  public function revisionIds(SystemEntityInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as System entity author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   System entity revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

}
