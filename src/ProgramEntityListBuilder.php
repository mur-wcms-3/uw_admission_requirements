<?php

namespace Drupal\uw_admission_requirements;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Program entity entities.
 *
 * @ingroup uw_admission_requirements
 */
class ProgramEntityListBuilder extends EntityListBuilder {

  /**
   * {@inheritDoc}
   */
  public function load() {

    $entity_query = \Drupal::service('entity.query')->get('program_entity');
    $header = $this->buildHeader();

    $entity_query->pager(50);
    $entity_query->tableSort($header);

    $uids = $entity_query->execute();

    return $this->storage->loadMultiple($uids);
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [
      'name' => [
        'data' => $this->t('Program'),
        'field' => 'name',
        'specifier' => 'name',
        'class' => [RESPONSIVE_PRIORITY_LOW],
      ],
      'codename' => [
        'data' => $this->t('URL codename'),
        'field' => 'codename',
        'specifier' => 'codename',
        'class' => [RESPONSIVE_PRIORITY_LOW],
      ],
    ];

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.program_entity.edit_form',
      ['program_entity' => $entity->id()]
    );
    $row['codename'] = $entity->codename->value;
    return $row + parent::buildRow($entity);
  }

}
