<?php

namespace Drupal\uw_admission_requirements;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\uw_admission_requirements\Entity\RequirementEntityInterface;

/**
 * Defines the storage handler class for Requirement entity entities.
 *
 * This extends the base storage class, adding required special handling for
 * Requirement entity entities.
 *
 * @ingroup uw_admission_requirements
 */
class RequirementEntityStorage extends SqlContentEntityStorage implements RequirementEntityStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(RequirementEntityInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {requirement_entity_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {requirement_entity_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

}
