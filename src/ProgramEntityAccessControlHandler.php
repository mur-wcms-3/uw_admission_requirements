<?php

namespace Drupal\uw_admission_requirements;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Program entity entity.
 *
 * @see \Drupal\uw_admission_requirements\Entity\ProgramEntity.
 */
class ProgramEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\uw_admission_requirements\Entity\ProgramEntityInterface $entity */
    switch ($operation) {

      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished program entity entities');
        }

        return AccessResult::allowedIfHasPermission($account, 'view published program entity entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit program entity entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete program entity entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add program entity entities');
  }

}
