<?php

namespace Drupal\uw_admission_requirements\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\NodeInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AdmissionsController.
 */
class AdmissionRequirementsController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->state = $container->get('state');
    $instance->messenger = $container->get('messenger');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->formBuilder = $container->get('form_builder');
    $instance->configFactory = $container->get('config.factory');
    return $instance;
  }

  /**
   * System Requirements.
   *
   * @param string $program_codename
   *   Codename of the program to lookup.
   * @param string $system_codename
   *   Codename of the system to lookup.
   * @param string $apib
   *   Whether to show ap or ib content.
   *
   * @return array
   *   Return.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function systemRequirements($program_codename = NULL, $system_codename = NULL, $apib = NULL) {

    if ($apib == 'ib') {
      $system_codename = 'ib';
    }

    // Lookup program matching url route.
    $pid = $this->entityTypeManager->getStorage('program_entity')->getQuery()
      ->condition('status', NodeInterface::PUBLISHED)
      ->condition('codename', $program_codename)
      ->execute();
    // Lookup system matching url route.
    $sid = $this->entityTypeManager->getStorage('system_entity')->getQuery()
      ->condition('status', NodeInterface::PUBLISHED)
      ->condition('codename', $system_codename)
      ->execute();

    // 404 if either value is not found.
    if (empty($pid) || empty($sid)) {
      throw new NotFoundHttpException();
    }

    // Load entities matching url route.
    $program_entity = $this->entityTypeManager->getStorage('program_entity')->load(key($pid));
    $system_entity = $this->entityTypeManager->getStorage('system_entity')->load(key($sid));

    // Query for the requirements matching the program and system ids provided.
    $query = $this->entityTypeManager->getStorage('requirement_entity')->getQuery();

    // Match global system requirements, regardless or program.
    $group1 = $query->andConditionGroup()
      ->notExists('program_id')
      ->condition('system_id', key($sid));

    // Match global program requirements, regardless of system.
    $group2 = $query->andConditionGroup()
      ->notExists('system_id')
      ->condition('program_id', key($pid));

    // Match exact program and system pairs.
    $group3 = $query->andConditionGroup()
      ->condition('system_id', key($sid))
      ->condition('program_id', key($pid));

    // Combine our conditions into an OR group.
    $orGroup = $query->orConditionGroup()
      ->condition($group1)
      ->condition($group2)
      ->condition($group3);

    // Ensure requirement is published and execute.
    $requirement_ids = $query
      ->condition('status', NodeInterface::PUBLISHED)
      ->condition($orGroup)
      ->execute();

    // Load the requirements entities found.
    $requirement_entities = $this->entityTypeManager->getStorage('requirement_entity')->loadMultiple($requirement_ids);

    $markup = "";
    $ap_requirement = NULL;
    foreach ($requirement_entities as $requirement) {
      $type = $requirement->type->entity->id();
      $current_req = [
        'name' => $requirement->type->entity->page_label(),
        'description' => $requirement->description->value,
        'weight' => $requirement->type->entity->weight(),
      ];

      // Handle special requirement types.
      if ($requirement->type->entity->is_special()) {
        // Handle AP Requirements.
        if ($apib == "ap" && $type == 'advanced_placement') {
          $ap_requirement = $current_req;
        }

        // Skip processing the requirement, we'll deal with them individually.
        continue;
      }

      // Add the current item to the requirements array.
      $requirements[] = $current_req;
    }

    /*
     * Sort requirements by requirement_type weight.
     * For some reason entityQuery cannot sort on a bundle base field (weight)
     * so we have to sort requirements after the query is complete. No biggie.
     */
    if (!empty($requirements)) {
      uasort($requirements, function ($a, $b) {
        if ($a['weight'] == $b['weight']) {
          return 0;
        }
        return ($a['weight'] < $b['weight']) ? -1 : 1;
      });
    }
    else {
      $requirements = [];
    }

    // Build render array with custom template (admission-requirement.html.twig)
    // We pass our content in attributes for the template to use.
    $build['requirements'] = [
      '#theme' => 'admission_requirement',
      '#content' => $markup,
      '#attributes' => [
        'program_name' => $program_entity->name,
        'system_name' => $system_entity->name->value,
        'is_international' => $system_entity->is_international,
        'requirements' => $requirements,
        'ap_requirement' => $ap_requirement,
        'program_notes' => $program_entity->field_program_notes,
        'more_info' => $program_entity->field_for_more_information,
      ],
    ];

    return $build;
  }

  /**
   * International Requirements.
   *
   * @param string $program_codename
   *   Codename.
   * @param string $country
   *   Country.
   *
   * @return array|string
   *   Array or string returned.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function internationalRequirements($program_codename = NULL, $country = NULL) {
    // Lookup program matching url route.
    $pid = $this->entityTypeManager->getStorage('program_entity')->getQuery()
      ->condition('status', NodeInterface::PUBLISHED)
      ->condition('codename', $program_codename)
      ->execute();

    // Lookup system matching url route.
    $sid = $this->entityTypeManager->getStorage('system_entity')->getQuery()
      ->condition('status', NodeInterface::PUBLISHED)
      ->condition('codename', 'international')
      ->execute();

    // 404 if either value is not found.
    if (empty($pid) || empty($sid)) {
      throw new NotFoundHttpException();
    }

    // Load entities matching url route.
    $program_entity = $this->entityTypeManager->getStorage('program_entity')->load(key($pid));
    $system_entity = $this->entityTypeManager->getStorage('system_entity')->load(key($sid));

    // Query for the requirements matching the program and system ids provided.
    $query = $this->entityTypeManager->getStorage('requirement_entity')->getQuery();

    $group1 = $query->andConditionGroup()
      ->notExists('program_id')
      ->condition('system_id', key($sid));

    $group2 = $query->andConditionGroup()
      ->notExists('system_id')
      ->condition('program_id', key($pid));

    $group3 = $query->andConditionGroup()
      ->condition('system_id', key($sid))
      ->condition('program_id', key($pid));

    $orGroup = $query->orConditionGroup()
      ->condition('codename', $country)
      ->condition($group1)
      ->condition($group2)
      ->condition($group3);

    $requirement_ids = $query
      ->condition('status', NodeInterface::PUBLISHED)
      ->condition($orGroup)
      ->execute();

    // Load the requirements entities found.
    $requirement_entities = $this->entityTypeManager->getStorage('requirement_entity')->loadMultiple($requirement_ids);
    if (empty($requirement_entities)) {
      return [
        'content' => [
          '#markup' => "<h3>No requirements found</h3>",
        ],
      ];
    }

    $markup = "";
    foreach ($requirement_entities as $requirement) {
      $req_type = $requirement->type->entity->id();
      $label = $requirement->type->entity->page_label();

      // Filter out other country entities.
      if ($req_type == 'country' && $requirement->codename->value != $country) {
        continue;
      }
      // Set title for country requirements.
      if ($req_type == 'country') {
        $label = "General Requirements";
      }

      // Handle special requirement types.
      if ($req_type != 'country' &&  $requirement->type->entity->is_special()) {
        continue;
      }

      // Add to requirements array.
      $requirements[] = [
        'name' => $label,
        'description' => $requirement->description->value,
        'weight' => $requirement->type->entity->weight(),
      ];
    }

    /*
     * Sort requirements by requirement_type weight.
     * For some reason entityQuery cannot sort on a bundle base field (weight)
     * so we have to sort requirements after the query is complete. No biggie.
     */
    uasort($requirements, function ($a, $b) {
      if ($a['weight'] == $b['weight']) {
        return 0;
      }
      return ($a['weight'] < $b['weight']) ? -1 : 1;
    });

    // Get the country name proper.
    $country_id = $this->entityTypeManager->getStorage('requirement_entity')->getQuery()
      ->condition('status', NodeInterface::PUBLISHED)
      ->condition('codename', $country)
      ->condition('type', 'country')
      ->execute();
    $country_entity = $this->entityTypeManager
      ->getStorage('requirement_entity')
      ->load(key($country_id));
    $country_name = $country_entity->name->value;

    // Build render array with custom template (admission-requirement.html.twig)
    // We pass our content in attributes for the template to use.
    $build['intro'] = [
      '#theme' => 'admission_requirement',
      '#content' => $markup,
      '#attributes' => [
        'program_name' => $program_entity->name,
        'system_name' => $country_name,
        'country' => $country,
        'requirements' => $requirements,
        'program_notes' => $program_entity->field_program_notes,
        'more_info' => $program_entity->field_for_more_information,
        'is_international' => $system_entity->is_international,
      ],
    ];

    return $build;
  }

  /**
   * Transfer Requirements.
   *
   * @param string $type
   *   Type.
   * @param string $program_codename
   *   Program Codename.
   * @param string $system_codename
   *   System Codename.
   * @param string $country
   *   Country.
   *
   * @return array
   *   Returns array. These forced comments are useless.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function transferRequirements($type = NULL, $program_codename = NULL, $system_codename = NULL, $country = NULL) {
    $build = [];
    // Lookup program matching url route.
    $pid = $this->entityTypeManager->getStorage('program_entity')->getQuery()
      ->condition('status', NodeInterface::PUBLISHED)
      ->condition('codename', $program_codename)
      ->execute();
    // Lookup system matching url route.
    $sid = $this->entityTypeManager->getStorage('system_entity')->getQuery()
      ->condition('status', NodeInterface::PUBLISHED)
      ->condition('codename', $system_codename)
      ->execute();

    // 404 if either value is not found.
    if (empty($pid) || empty($sid)) {
      throw new NotFoundHttpException();
    }

    // Load entities matching url route.
    $program_entity = $this->entityTypeManager->getStorage('program_entity')->load(key($pid));
    $system_entity = $this->entityTypeManager->getStorage('system_entity')->load(key($sid));

    // Query for the requirements matching the program and system ids provided.
    $query = $this->entityTypeManager->getStorage('requirement_entity')->getQuery();

    // Match global system requirements, regardless or program.
    $group1 = $query->andConditionGroup()
      ->notExists('program_id')
      ->condition('system_id', key($sid));

    // Match global program requirements, regardless of system.
    $group2 = $query->andConditionGroup()
      ->notExists('system_id')
      ->condition('program_id', key($pid));

    // Match exact program and system pairs.
    $group3 = $query->andConditionGroup()
      ->condition('system_id', key($sid))
      ->condition('program_id', key($pid));

    // Combine our conditions into an OR group.
    $orGroup = $query->orConditionGroup()
      ->condition($group1)
      ->condition($group2)
      ->condition($group3);

    // Ensure requirement is published and execute.
    $requirement_ids = $query
      ->condition('status', NodeInterface::PUBLISHED)
      ->condition($orGroup)
      ->execute();

    // Load the requirements entities found.
    $requirement_entities = $this->entityTypeManager->getStorage('requirement_entity')->loadMultiple($requirement_ids);

    $markup = "";
    $transfer_requirement = NULL;
    foreach ($requirement_entities as $requirement) {
      $req_type = $requirement->type->entity->id();
      $current_req = [
        'name' => $requirement->type->entity->page_label(),
        'description' => $requirement->description->value,
        'weight' => $requirement->type->entity->weight(),
      ];

      // Skip general requirements on college transfers.
      if ($req_type == 'general' && $type == 'college') {
        continue;
      }

      // Skip general admission average for system transfers.
      if ($country == NULL && $req_type == 'admission_average') {
        continue;
      }

      // Handle special requirement types.
      if ($requirement->type->entity->is_special()) {
        // Handle College Transfer Requirements.
        if ($req_type == 'college_transfer_credits' && $type == 'college') {
          $transfer_requirement = $current_req;
        }

        if ($req_type == 'college_transfer_average' && $type == 'college') {
          $transfer_average_requirement = $current_req;
        }

        if ($req_type == 'university_transfer_credits' && $type == 'university') {
          $transfer_requirement = $current_req;
        }
        if ($req_type == 'university_transfer_average' && $type == 'university') {
          $transfer_average_requirement = $current_req;
        }

        // Filter out other country entities.
        if ($req_type == 'country' && $requirement->codename->value != $country) {
          continue;
        }
        // Set title for country requirements.
        if ($req_type == 'country') {
          $current_req['name'] = "General Requirements";
          $requirements[] = $current_req;
        }

        // Skip processing the requirement, we'll deal with them individually.
        continue;
      }

      // Add the current item to the requirements array.
      $requirements[] = $current_req;
    }

    /*
     * Sort requirements by requirement_type weight.
     * For some reason entityQuery cannot sort on a bundle base field (weight)
     * so we have to sort requirements after the query is complete. No biggie.
     */
    if (!empty($requirements)) {
      uasort($requirements, function ($a, $b) {
        if ($a['weight'] == $b['weight']) {
          return 0;
        }
        return ($a['weight'] < $b['weight']) ? -1 : 1;
      });
    }
    else {
      $requirements = [];
    }

    if (!empty($country)) {
      // Get the country name proper.
      $country_id = $this->entityTypeManager->getStorage('requirement_entity')->getQuery()
        ->condition('status', NodeInterface::PUBLISHED)
        ->condition('codename', $country)
        ->condition('type', 'country')
        ->execute();
      $country_entity = $this->entityTypeManager
        ->getStorage('requirement_entity')
        ->load(key($country_id));
      $country_name = $country_entity->name->value;

      if ($type == 'college') {
        $system_name = "$country_name college transfer";
      }
      else {
        $system_name = "$country_name university transfer";
      }
    }
    else {
      if ($type == 'college') {
        $system_name = $system_entity->name->value . " college transfer";
      }
      else {
        $system_name = $system_entity->name->value . " university transfer";
      }
    }

    // Build render array with custom template (admission-requirement.html.twig)
    // We pass our content in attributes for the template to use.
    $build['requirements'] = [
      '#theme' => 'admission_requirement',
      '#content' => $markup,
      '#attributes' => [
        'is_transfer' => TRUE,
        'program_name' => $program_entity->name,
        'system_name' => $system_name,
        'is_international' => $system_entity->is_international,
        'requirements' => $requirements,
        'transfer_average' => !empty($transfer_average_requirement) ? $transfer_average_requirement : '',
        'transfer_general' => $transfer_requirement,
        'program_notes' => $program_entity->field_program_notes,
        'more_info' => $program_entity->field_for_more_information,
      ],
    ];

    return $build;
  }

  /**
   * Shows the non typical requirements pages.
   *
   * @param string $student_type
   *   Student Type.
   *
   * @return array|\Symfony\Component\HttpFoundation\RedirectResponse|void
   *   Returns array.
   */
  public function nonTypicalRequirements($student_type = NULL) {

    switch ($student_type) {
      case 'ged':
        if ($this->state->get('uw_admission_requirements.ged_use_redirect')) {
          $redirect = $this->state->get('uw_admission_requirements.ged_redirect');
          $url = Url::fromUri('entity:node/' . $redirect)->getRouteName();
          return $this->redirect($url, ['node' => $redirect]);
        }
        $title = $this->state->get('uw_admission_requirements.ged_page_title', '');
        $page_content = $this->state->get('uw_admission_requirements.ged_page_content', '');
        break;

      case 'home-schooled-students';
        if ($this->state->get('uw_admission_requirements.home_schooled_use_redirect')) {
          $redirect = $this->state->get('uw_admission_requirements.home_schooled_redirect');
          $url = Url::fromUri('entity:node/' . $redirect)->getRouteName();
          return $this->redirect($url, ['node' => $redirect]);
        }
        $title = $this->state->get('uw_admission_requirements.home_schooled_page_title', '');
        $page_content = $this->state->get('uw_admission_requirements.home_schooled_page_content', '');
        break;

      case 'mature-student';
        if ($this->state->get('uw_admission_requirements.mature_use_redirect')) {
          $redirect = $this->state->get('uw_admission_requirements.mature_redirect');
          $url = Url::fromUri('entity:node/' . $redirect)->getRouteName();
          return $this->redirect($url, ['node' => $redirect]);
        }
        $title = $this->state->get('uw_admission_requirements.mature_page_title', '');
        $page_content = $this->state->get('uw_admission_requirements.mature_page_content', '');
        break;

      default:
        throw new NotFoundHttpException();
    }

    return [
      '#theme' => "non_typical_requirement",
      '#content' => $page_content,
      '#attributes' => [
        'title' => $title,
      ],
    ];

  }

  /**
   * Shows the requirements form on a page.
   *
   * @return array
   *   Array.
   */
  public function showRequirementsForm() {
    $build = [];

    $config = $this->configFactory->get('uw_admission_requirements.settings');

    $build['header'] = [
      '#markup' => '<h2>Admission Requirements</h2>' . $config->get('student-type.high-school-student'),
    ];

    $build['form'] = $this->formBuilder->getForm('Drupal\uw_admission_requirements\Form\AdmissionRequirementsForm');
    return $build;
  }

}
