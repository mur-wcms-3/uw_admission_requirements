<?php

namespace Drupal\uw_admission_requirements\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\uw_admission_requirements\Entity\ProgramEntityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Serialization\Json;

/**
 * Class ProgramEntityController.
 *
 *  Returns responses for Program entity routes.
 */
class ProgramEntityController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->renderer = $container->get('renderer');
    $instance->bundleInfo = $container->get('entity_type.bundle.info');
    return $instance;
  }

  /**
   * Generates a page with an embedded view of all requirements for program.
   *
   * @param \Drupal\uw_admission_requirements\Entity\ProgramEntityInterface $program_entity
   *   A Program entity object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function requirementsShow(ProgramEntityInterface $program_entity) {
    $build = [];
    $build['#title'] = $this->t('Requirements for %title', ['%title' => $program_entity->label()]);
    // Include libraries for modal pop up display.
    $build['#attached']['library'] = [
      'core/drupal.dialog.ajax',
      'core/jquery.form',
    ];

    // Get bundle types for requirement entities.
    $bundles = $this->bundleInfo->getBundleInfo('requirement_entity');
    // Add buttons to create each requirement type at the top of the page.
    $links = [];
    unset($bundles['country']);

    // Sort the bundles for the dropdown.
    uasort($bundles, function ($a, $b) {
      if ($a['label'] == $b['label']) {
        return 0;
      }
      return ($a['label'] < $b['label']) ? -1 : 1;
    });

    foreach ($bundles as $key => $bundle) {
      $label = $bundle['label'];
      $links[] = [
        'title' => $label,
        'url' => Url::fromRoute('entity.requirement_entity.add_form',
          ['requirement_entity_type' => $key],
          ['query' => array_merge($this->getDestinationArray(), ['program_id' => $program_entity->id()])]
        ),
        'attributes' => [
          'class' => ['use-ajax'],
          'data-dialog-type' => 'modal',
          'data-dialog-options' => Json::encode(['width' => 700]),
        ],
      ];
    }

    $build['container'] = [
      '#type' => 'actions',
      '#attributes' => [
        'style' => 'width: max-content;',
      ],
    ];
    $build['container']['actions'] = [
      '#type' => 'operations',
      '#links' => $links,
      '#prefix' => '<strong>Select a requirement type to add</strong>',
    ];

    $build['spacer'] = [
      '#markup' => '<br><hr>',
    ];

    // Load the program requirements view and embed it.
    $build['requirements'] = [
      '#type' => 'view',
      '#name' => 'requirement_entities',
      '#display_id' => 'program',
      '#arguments' => [
        'arg_0' => $program_entity->id(),
      ],
    ];

    return $build;
  }

  /**
   * Displays a Program entity revision.
   *
   * @param int $program_entity_revision
   *   The Program entity revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($program_entity_revision) {
    $program_entity = $this->entityTypeManager()->getStorage('program_entity')
      ->loadRevision($program_entity_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('program_entity');

    return $view_builder->view($program_entity);
  }

  /**
   * Page title callback for a Program entity revision.
   *
   * @param int $program_entity_revision
   *   The Program entity revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($program_entity_revision) {
    $program_entity = $this->entityTypeManager()->getStorage('program_entity')
      ->loadRevision($program_entity_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $program_entity->label(),
      '%date' => $this->dateFormatter->format($program_entity->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Program entity.
   *
   * @param \Drupal\uw_admission_requirements\Entity\ProgramEntityInterface $program_entity
   *   A Program entity object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(ProgramEntityInterface $program_entity) {
    $account = $this->currentUser();
    $program_entity_storage = $this->entityTypeManager()->getStorage('program_entity');

    $build['#title'] = $this->t('Revisions for %title', ['%title' => $program_entity->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all program entity revisions") || $account->hasPermission('administer program entity entities')));
    $delete_permission = (($account->hasPermission("delete all program entity revisions") || $account->hasPermission('administer program entity entities')));

    $rows = [];

    $vids = $program_entity_storage->revisionIds($program_entity);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\uw_admission_requirements\ProgramEntityInterface $revision */
      $revision = $program_entity_storage->loadRevision($vid);
      $username = [
        '#theme' => 'username',
        '#account' => $revision->getRevisionUser(),
      ];

      // Use revision link to link to revisions that are not active.
      $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
      if ($vid != $program_entity->getRevisionId()) {
        $link = $this->l($date, new Url('entity.program_entity.revision', [
          'program_entity' => $program_entity->id(),
          'program_entity_revision' => $vid,
        ]));
      }
      else {
        $link = $program_entity->link($date);
      }

      $row = [];
      $column = [
        'data' => [
          '#type' => 'inline_template',
          '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
          '#context' => [
            'date' => $link,
            'username' => $this->renderer->renderPlain($username),
            'message' => [
              '#markup' => $revision->getRevisionLogMessage(),
              '#allowed_tags' => Xss::getHtmlTagList(),
            ],
          ],
        ],
      ];
      $row[] = $column;

      if ($latest_revision) {
        $row[] = [
          'data' => [
            '#prefix' => '<em>',
            '#markup' => $this->t('Current revision'),
            '#suffix' => '</em>',
          ],
        ];
        foreach ($row as &$current) {
          $current['class'] = ['revision-current'];
        }
        $latest_revision = FALSE;
      }
      else {
        $links = [];
        if ($revert_permission) {
          $links['revert'] = [
            'title' => $this->t('Revert'),
            'url' => Url::fromRoute('entity.program_entity.revision_revert', [
              'program_entity' => $program_entity->id(),
              'program_entity_revision' => $vid,
            ]),
          ];
        }

        if ($delete_permission) {
          $links['delete'] = [
            'title' => $this->t('Delete'),
            'url' => Url::fromRoute('entity.program_entity.revision_delete', [
              'program_entity' => $program_entity->id(),
              'program_entity_revision' => $vid,
            ]),
          ];
        }

        $row[] = [
          'data' => [
            '#type' => 'operations',
            '#links' => $links,
          ],
        ];
      }

      $rows[] = $row;
    }

    $build['program_entity_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
