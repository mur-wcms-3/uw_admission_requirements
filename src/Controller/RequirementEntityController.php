<?php

namespace Drupal\uw_admission_requirements\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\uw_admission_requirements\Entity\RequirementEntityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class RequirementEntityController.
 *
 *  Returns responses for Requirement entity routes.
 */
class RequirementEntityController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * Displays a Requirement entity revision.
   *
   * @param int $requirement_entity_revision
   *   The Requirement entity revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($requirement_entity_revision) {
    $requirement_entity = $this->entityTypeManager()->getStorage('requirement_entity')
      ->loadRevision($requirement_entity_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('requirement_entity');

    return $view_builder->view($requirement_entity);
  }

  /**
   * Page title callback for a Requirement entity revision.
   *
   * @param int $requirement_entity_revision
   *   The Requirement entity revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($requirement_entity_revision) {
    $requirement_entity = $this->entityTypeManager()->getStorage('requirement_entity')
      ->loadRevision($requirement_entity_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $requirement_entity->label(),
      '%date' => $this->dateFormatter->format($requirement_entity->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Requirement entity.
   *
   * @param \Drupal\uw_admission_requirements\Entity\RequirementEntityInterface $requirement_entity
   *   A Requirement entity object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(RequirementEntityInterface $requirement_entity) {
    $account = $this->currentUser();
    $requirement_entity_storage = $this->entityTypeManager()->getStorage('requirement_entity');

    $build['#title'] = $this->t('Revisions for %title', ['%title' => $requirement_entity->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all requirement entity revisions") || $account->hasPermission('administer requirement entity entities')));
    $delete_permission = (($account->hasPermission("delete all requirement entity revisions") || $account->hasPermission('administer requirement entity entities')));

    $rows = [];

    $vids = $requirement_entity_storage->revisionIds($requirement_entity);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\uw_admission_requirements\RequirementEntityInterface $revision */
      $revision = $requirement_entity_storage->loadRevision($vid);
      $username = [
        '#theme' => 'username',
        '#account' => $revision->getRevisionUser(),
      ];

      // Use revision link to link to revisions that are not active.
      $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
      if ($vid != $requirement_entity->getRevisionId()) {
        $link = $this->l($date, new Url('entity.requirement_entity.revision', [
          'requirement_entity' => $requirement_entity->id(),
          'requirement_entity_revision' => $vid,
        ]));
      }
      else {
        $link = $requirement_entity->link($date);
      }

      $row = [];
      $column = [
        'data' => [
          '#type' => 'inline_template',
          '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
          '#context' => [
            'date' => $link,
            'username' => $this->renderer->renderPlain($username),
            'message' => [
              '#markup' => $revision->getRevisionLogMessage(),
              '#allowed_tags' => Xss::getHtmlTagList(),
            ],
          ],
        ],
      ];
      $row[] = $column;

      if ($latest_revision) {
        $row[] = [
          'data' => [
            '#prefix' => '<em>',
            '#markup' => $this->t('Current revision'),
            '#suffix' => '</em>',
          ],
        ];
        foreach ($row as &$current) {
          $current['class'] = ['revision-current'];
        }
        $latest_revision = FALSE;
      }
      else {
        $links = [];
        if ($revert_permission) {
          $links['revert'] = [
            'title' => $this->t('Revert'),
            'url' => Url::fromRoute('entity.requirement_entity.revision_revert', [
              'requirement_entity' => $requirement_entity->id(),
              'requirement_entity_revision' => $vid,
            ]),
          ];
        }

        if ($delete_permission) {
          $links['delete'] = [
            'title' => $this->t('Delete'),
            'url' => Url::fromRoute('entity.requirement_entity.revision_delete', [
              'requirement_entity' => $requirement_entity->id(),
              'requirement_entity_revision' => $vid,
            ]),
          ];
        }

        $row[] = [
          'data' => [
            '#type' => 'operations',
            '#links' => $links,
          ],
        ];
      }

      $rows[] = $row;
    }

    $build['requirement_entity_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
