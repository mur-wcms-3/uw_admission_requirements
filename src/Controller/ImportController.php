<?php

namespace Drupal\uw_admission_requirements\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Import Controller.
 */
class ImportController extends ControllerBase {

  /**
   * Drupal\Core\Entity\EntityManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityManager = $container->get('entity.manager');
    return $instance;
  }

  /**
   * Import systems.
   *
   * @return array
   *   Return Hello string.
   */
  public function importSystems() {
    return [
      '#type' => 'markup',
      '#markup' => $this->t('TODO: Implement system import functionality'),
    ];
  }

  /**
   * Import programs.
   *
   * @return array
   *   Return Hello string.
   */
  public function importPrograms() {
    return [
      '#type' => 'markup',
      '#markup' => $this->t('TODO: Implement program import functionality'),
    ];
  }

}
