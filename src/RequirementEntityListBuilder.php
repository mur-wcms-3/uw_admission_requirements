<?php

namespace Drupal\uw_admission_requirements;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Requirement entity entities.
 *
 * @ingroup uw_admission_requirements
 */
class RequirementEntityListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [
      'page_label' => [
        'data' => $this->t('Page Label'),
        'field' => 'page_label',
        'specifier' => 'page_label',
        'class' => [RESPONSIVE_PRIORITY_LOW],
      ],
      'type' => [
        'data' => $this->t('Type'),
        'field' => 'type',
        'specifier' => 'type',
        'class' => [RESPONSIVE_PRIORITY_LOW],
      ],
      'program' => [
        'data' => $this->t('Programs'),
        'field' => 'program',
        'specifier' => 'program',
        'class' => [RESPONSIVE_PRIORITY_LOW],
      ],
      'system' => [
        'data' => $this->t('Systems'),
        'field' => 'system',
        'specifier' => 'system',
        'class' => [RESPONSIVE_PRIORITY_LOW],
      ],
    ];

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.requirement_entity.edit_form',
      ['requirement_entity' => $entity->id()]
    );

    $row['type'] = $entity->type->entity->label();

    $row['program'] = $this::getProgramsRow($entity) ?: '';
    $row['system'] = $this::getSystemsRow($entity) ?: '';

    return $row + parent::buildRow($entity);
  }

  /**
   * Return a list of program names associated with the entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity.
   *
   * @return array
   *   Programs.
   */
  private static function getProgramsRow(EntityInterface $entity) {
    $programs = [];

    // Loop through each program id.
    if ($entity->hasField('program_id')) {
      foreach ($entity->program_id->referencedEntities() as $program) {
        // Get a link to the program referenced.
        $link = Link::createFromRoute(
          $program->label(),
          'entity.program_entity.edit_form',
          ['program_entity' => $program->id()]);

        // Add the program link and a comma after it to a render array.
        $programs[] = $link->toRenderable();
        $programs[] = ['#markup' => ', '];
      }
      // Removes the last comma from the list.
      array_pop($programs);

      // Renders the array for use in the list builder.
      $programs = \Drupal::service('renderer')->render($programs);
    }

    return $programs;
  }

  /**
   * Return a list of system names associated with the entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity.
   *
   * @return array
   *   Systems.
   */
  private static function getSystemsRow(EntityInterface $entity) {
    $systems = [];

    // Loop through each system id.
    if ($entity->hasField('system_id')) {
      foreach ($entity->system_id->referencedEntities() as $system) {
        // Get a link to the program referenced.
        $link = Link::createFromRoute(
          $system->label(),
          'entity.system_entity.edit_form',
          ['system_entity' => $system->id()]);

        // Add the system link and a comma after it to a render array.
        $systems[] = $link->toRenderable();
        $systems[] = ['#markup' => ', '];
      }
      // Removes the last comma from the list.
      array_pop($systems);

      // Renders the array for use in the list builder.
      $systems = \Drupal::service('renderer')->render($systems);
    }

    return $systems;
  }

}
