// This makes a group of checkboxes behave like radios, with the added
// functionality of being able to uncheck them all.
// The checkboxes must have the same classes.
jQuery(document).ready(function ($) {
  $("input:checkbox").click(function () {
    var group = "input:checkbox[class='" + $(this).prop("class") + "']";
    if ($(this).prop('checked') == true) {
      $(group).prop("checked",false);
      $(this).prop("checked",true);
    } else {
      $(this).prop("checked", false);
    }
  });
});
