<?php

/**
 * @file
 * Contains system_entity.page.inc.
 *
 * Page callback for System entity entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for System entity templates.
 *
 * Default template: system_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_system_entity(array &$variables) {
  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
