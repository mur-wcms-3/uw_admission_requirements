<?php

/**
 * @file
 * Contains requirement_entity.page.inc.
 *
 * Page callback for Requirement entity entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Requirement entity templates.
 *
 * Default template: requirement_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_requirement_entity(array &$variables) {
  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
