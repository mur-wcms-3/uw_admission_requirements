<?php

/**
 * @file
 * Contains program_entity.page.inc.
 *
 * Page callback for Program entity entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Program entity templates.
 *
 * Default template: program_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_program_entity(array &$variables) {
  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
